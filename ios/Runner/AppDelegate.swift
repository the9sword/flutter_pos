import UIKit
import Flutter
import AVFoundation
@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
     var myOrientation: UIInterfaceOrientationMask = .all
    
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
      
        let batteryChannel = FlutterMethodChannel(name: "samples.flutter.io/battery",
                                                  binaryMessenger: controller)
        batteryChannel.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: FlutterResult) -> Void in
            guard call.method == "getBatteryLevel" else {
                result(FlutterMethodNotImplemented)
                return
            }
            
            self!.receiveBatteryLevel(result: result)
        })
        
        
        let orientationMode = FlutterMethodChannel(name: "samples.flutter.io/orientation",
                                                  binaryMessenger: controller)
        orientationMode.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: FlutterResult) -> Void in
            guard call.method == "setOrientationMode" else {
                result(FlutterMethodNotImplemented)
                return
            }
            
            self!.lockScreen(result: result)
        })
        
        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }

//    func proceedWithCameraAccess(identifier: String){
//        // handler in .requestAccess is needed to process user's answer to our request
//        AVCaptureDevice.requestAccess(for: .video) { success in
//            if success { // if request is granted (success is true)
//                DispatchQueue.main.async {
//                    self.performSegue(withIdentifier: identifier, sender: nil)
//                }
//            } else { // if request is denied (success is false)
//                // Create Alert
//                let alert = UIAlertController(title: "Camera", message: "Camera access is absolutely necessary to use this app", preferredStyle: .alert)
//
//                // Add "OK" Button to alert, pressing it will bring you to the settings app
//                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
//                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
//                }))
//                // Show the alert with animation
//                self.present(alert, animated: true)
//            }
//        }
//    }
    
    private func lockScreen(result: FlutterResult) {
         myOrientation = .landscape
        print("ssssssssssssss");
  
    }
override func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return myOrientation
}
    
    
private func receiveBatteryLevel(result: FlutterResult) {
      
    result(Int(UIScreen.main.bounds.width))
//    let device = UIDevice.current
//    device.isBatteryMonitoringEnabled = true
//    if device.batteryState == UIDeviceBatteryState.unknown {
//        result(FlutterError(code: "UNAVAILABLE",
//                            message: "Battery info unavailable",
//                            details: nil))
//    } else {
//        result(Int(device.batteryLevel * 100))
//    }
}
}

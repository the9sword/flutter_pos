import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:path/path.dart' as path;
import 'package:pos/Dao/Product.dart';
import 'DatabaseCall.dart';
import 'package:pos/firstFragment.dart';
import 'package:sqflite/sqflite.dart';
import 'package:pos/OrderDetailsFragment.dart';
import 'package:pos/Dao/Order.dart';
import 'package:pos/Dao/OrderItem.dart';
import 'package:pos/Dao/ResTable.dart';
import 'MyState.dart';

void main() {
  //screen lock
  SystemChrome.setPreferredOrientations([
  //  DeviceOrientation.portraitUp,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight,
  ]).then((_) {
    runApp(new MyApp());
  });
}

SqfliteAdapter _adapter;

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'POS'),
    );
  }
}

void setupDatabase() async {
 return ;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();

    final bean = ProductBean(_adapter);
    final orderBean = OrderBean(_adapter);
    final orderItemBean = OrderItemBean(_adapter);
    final tableBean =ResTableBean(_adapter);



    await bean.drop();
    await orderBean.drop();
    await orderItemBean.drop();
    await tableBean.drop();

    await bean.createTable();
    await orderBean.createTable();
    await orderItemBean.createTable();
    await tableBean.createTable();

    await bean.removeAll();
    await orderBean.removeAll();
    await orderItemBean.removeAll();
    await tableBean.removeAll();



    await tableBean.insert(new ResTable.make(null, "1", true,0.01,0.5));
    await tableBean.insert(new ResTable.make(null, "2", true,0.18,0.5));
    await tableBean.insert(new ResTable.make(null, "3", true,0.28,0.5));
    await tableBean.insert(new ResTable.make(null, "4", true,0.37,0.5));
    await tableBean.insert(new ResTable.make(null, "5", true,0.44,0.5));

    await tableBean.insert(new ResTable.make(null, "6", true,0.47,0.42));
    await tableBean.insert(new ResTable.make(null, "7", true,0.41,0.42));
    await tableBean.insert(new ResTable.make(null, "8", true,0.34,0.42));
    await tableBean.insert(new ResTable.make(null, "9", true,0.24,0.42));
    await tableBean.insert(new ResTable.make(null, "10", true,0.17,0.42));
    await tableBean.insert(new ResTable.make(null, "11", true,0.167,0.335));
    await tableBean.insert(new ResTable.make(null, "12", true,0.234,0.335));
    await tableBean.insert(new ResTable.make(null, "13", true,0.31,0.335));
    await tableBean.insert(new ResTable.make(null, "14", true,0.38,0.335));
    await tableBean.insert(new ResTable.make(null, "15", true,0.45,0.335));



    await bean.insert(new Product.make(1, 'Nasi Lemak', Category.Rice.index,
        "mamak king", "img1.jpg", 11.20, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(7, 'Nasi Lemak 2', Category.Rice.index,
        "mamak king", "img1.jpg", 15.85, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(8, 'Nasi Lemak 3', Category.Rice.index,
        "mamak king", "img1.jpg", 12.99, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(9, 'Nasi Lemak 4', Category.Rice.index,
        "mamak king", "img1.jpg", 10.90, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(10, 'Nasi Lemak 5', Category.Rice.index,
        "mamak king", "img1.jpg", 12.80, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(2, 'Sky Juice', Category.Breverage.index,
        "mamak king2", "img2.jpg", 13.99, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(
        11,
        'Rock Juice',
        Category.Breverage.index,
        "mamak king2",
        "img2.jpg",
        12.00,
        true,
        null,
        null,
        DateTime.now()));
    await bean.insert(new Product.make(4, 'No Juice', Category.Breverage.index,
        "mamak king2", "img2.jpg", 1.00, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(5, 'Yes Juice', Category.Breverage.index,
        "mamak king2", "img2.jpg", 88.00, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(
        6,
        'Heaven Juice',
        Category.Breverage.index,
        "mamak king2",
        "img2.jpg",
        199.00,
        true,
        null,
        null,
        DateTime.now()));

    int id3 = await bean.insert(new Product.make(
        3,
        'Nasi Lemak Special',
        Category.Special.index,
        "mamak king",
        "img3.jpg",
        64.00,
        true,
        null,
        null,
        DateTime.now()));

    // int orderid =  await orderBean.insert(new Order.make(null,"nothing",1,null,OrderStatus.Order.index, DateTime.now()));

   // await orderItemBean.insert(new OrderItem.make(null,orderid,id3,"kurang manis",1,0,OrderStatus.Order.index, DateTime.now()));

   // Product post1 = await bean.find(id3);
  // print(post1.description);

    // getAllProductByCategory(Category.Noodle);
   // _adapter.close();
  } catch (e) {
    print("errorDatabase" + e.toString());
    //_adapter.close();
  }
}

class MyHomePage extends StatefulWidget {


  homeObservable observable;

  MyHomePage({Key key, this.title, this.stream}) : super(key: key);

  final Stream<int> stream;

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  MyHomePageState myState = MyHomePageState();

  @override
  MyHomePageState createState() {
    observable = new homeObservable(myState);
    return myState;
  }
}

class MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  Choice _selectedChoice = choices[0]; // The app's "state".

  void _select(Choice choice) {
    // Causes the app to rebuild with the new _selectedChoice.
    setState(() {
      _selectedChoice = choice;
    });
  }

  //StreamController<String> streamController1 = new StreamController.broadcast();   //Add .broadcast here
  var text = "aaaaaaa";

  int _counter = 0;
  bool landscapeMode;
  int foodGridCount = 5;

  double leftSize;
  bool isOpen=false;


  double width = 0;


  bool isTableModeSelected;
  int tableNo;
  FirstFragment firstFrag;
  OrderDetailFragment secondFrag;
  StreamController<int> onOrderChangeController;
  StreamController<MyState> _streamController;
  StreamController<int> _onActionBarTab;
  @override
  initState() {
    super.initState();

    onOrderChangeController = new StreamController.broadcast();
    _streamController = new StreamController.broadcast();
    _onActionBarTab = new StreamController.broadcast();
    firstFrag = new FirstFragment(widget.observable ,_streamController,_onActionBarTab, onOrderChangeController , foodGridCount);
    secondFrag = new OrderDetailFragment(_streamController, onOrderChangeController , widget.myState);

    _streamController.stream.listen((data) {
      tableNo = data.TableId;
    }, onDone: () {}, onError: (error) {});
  }

  void switchSize(int num) {

    _streamController.add(MyState.changeText(num , foodGridCount));
    setState(() {
       isOpen = false;
      leftSize = (width * (isOpen ? 0.95:0.65)).roundToDouble();
      foodGridCount = isOpen ? 4 : 6;
    });

    //secondFrag.showSnack();
  }

  _showDialog(BuildContext context ) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      //this right here
      child: Container(
        height: 300.0,
        width: 300.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(15.0),
              child: new Container(
                  width: 120.0,
                  height: 120.0,
                  decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: new NetworkImage(
                              "https://i.imgur.com/BoN9kdC.png")
                      )
                  )),
            ),
            Padding(
              padding: EdgeInsets.all(15.0),
              child: new RaisedButton(
                onPressed: () => {},
                color: Colors.blue,
                padding: EdgeInsets.all(4.0),
                child:
                    Text("Logout")

              ),
            ),
            Padding(padding: EdgeInsets.only(top: 50.0)),
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Role : ',
                  style: TextStyle(color: Colors.purple, fontSize: 18.0),
                ))
          ],
        ),
      ),
    );
  }

 bool runOnce = true;

  @override
  Widget build(BuildContext context) {
    if(runOnce) {
      width = MediaQuery
          .of(context)
          .size
          .width;
      leftSize = (width * 0.95).roundToDouble();


     // setState(() {
        runOnce = false;
     // });
    }


    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      // is portrait
      landscapeMode = false;
      print("isportrait");
      return buildPotraitMode();
    } else {
// is landscape
      landscapeMode = true;
      print("islandscape");
      return buildLandscapeMode();
    }
  }

  buildPotraitMode() {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),

    );
  }

  buildLandscapeMode() {
//    StreamBuilder(
//        stream: widget.stream,
//        builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
//
//          //return snapshot.hasData ? Text(snapshot.data.toString()) : Text('nodata');

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          // action button
          IconButton(
            icon: Icon(choices[0].icon),
            onPressed: () async{
              _select(choices[0]);
              if(await checkAddedOrderWithoutProcess(tableNo ?? -1)){
                _onActionBarTab.add(1);
               // switchSize(0);
              }

            },
          ),
          // action button
          IconButton(
            icon: Icon(choices[1].icon),
            onPressed: ()async {
              _select(choices[1]);
              if(await checkAddedOrderWithoutProcess(tableNo ?? -1)) {
                _onActionBarTab.add(2);
                switchSize(0);
              }
            },
          ),
          IconButton(
            icon: Icon(choices[2].icon),
            onPressed: () {
              _select(choices[2]);
              showDialog(
                  context: context,
                  builder: (_) => _showDialog(context)
              );
            },
          ),
          // overflow menu
//          PopupMenuButton<Choice>(
//            onSelected: _select,
//            itemBuilder: (BuildContext context) {
//              return choices.map((Choice choice) {
//                return PopupMenuItem<Choice>(
//                  value: choice,
//                  child: Text(choice.title),
//                );
//              }).toList();
//            },
//          ),
        ],
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new AnimatedSize(
            curve: Curves.fastOutSlowIn,
            child: new Container(
              child: firstFrag,
              width: leftSize ,
            ),
            vsync: this,
            duration: new Duration(milliseconds: 300),
          ),

          Container(width: 1, color: Colors.transparent),
          new AnimatedSize(
            curve: Curves.fastOutSlowIn,
            child:  new Container(
              child: secondFrag,
              width: width - leftSize - 1 ,
            ),
            vsync: this,
            duration: new Duration(milliseconds: 300),
          ),
        ],
      ),
    );
    //  });
  }
}

class homeObservable extends ValueNotifier<MyHomePageState> {
  homeObservable(MyHomePageState value) : super(value);

  void add(int val) {
    value.switchSize(val);
    //value.text = "aaaaaawwww";
   // value.temporaryTableId = 12;
   // var temp = value.temporaryTableId;
  //  print("ohhhhhhhh $temp");
    notifyListeners();
  }


}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Table', icon: Icons.restaurant),
  const Choice(title: 'Bill', icon: Icons.shopping_basket),
  const Choice(title: 'Profile', icon: Icons.person),
//  const Choice(title: 'Bus', icon: Icons.directions_bus),
//  const Choice(title: 'Train', icon: Icons.directions_railway),
//  const Choice(title: 'Walk', icon: Icons.directions_walk),
];

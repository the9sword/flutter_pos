import 'dart:async';

import 'package:flutter/material.dart';
//import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:pos/CustomTab.dart';
import 'package:pos/Dao/Product.dart';
import 'package:pos/DatabaseCall.dart';
import 'package:pos/MyState.dart';

class GridFragment extends StatefulWidget {
  int foodGridCount;
  int tableNo;
  final StreamController<MyState> streamController;

  GridFragment(this.streamController, this.foodGridCount, {Key key})
      : super(key: key);
  var state;

  @override
  _GridFragmentState createState() {
    streamController.stream.listen((data) {
      tableNo = data.TableId;
      foodGridCount = 4;
state.setNewState();
      print("DataReceived Grid: $tableNo");
    }, onDone: () {
      print("Task Done1");
    }, onError: (error) {
      print("Some Error1");
    });

    return state = _GridFragmentState();
  }
}

class _GridFragmentState<T extends StatefulWidget> extends State<GridFragment> {
  List<Product> list;
  List<Category> listTitle = [
    Category.Rice,
    Category.Dessert,
    Category.Breverage,
    Category.Special
  ];
  List<String> listImg = ["img1.jpg", "img3.jpg", "img2.jpg"];

  //Use _isInit to indicate if loadWidget is called for the first time
  bool _isInit;

  @override
  initState() {
    super.initState();
    _isInit = true;
    onLoadData(Category.Rice);
  }

  Choice _selectedChoice = choices[0]; // The app's "state".

  void _select(Choice choice) {
    // Causes the app to rebuild with the new _selectedChoice.
    setState(() {
      _selectedChoice = choice;
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Tap ${choice.title}'),
      ));
    });
  }

  setNewState() {
    setState(() {});
  }

  call() {
    onLoadData(Category.Rice);
  }

  _onTapImage(BuildContext context, String url) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        new Image.asset(
          "assets/" + url,
          fit: BoxFit.fill,
          height: 100,
        ), // Show your Image
        Align(
          alignment: Alignment.topRight,
          child: RaisedButton.icon(
              color: Theme.of(context).accentColor,
              textColor: Colors.white,
              onPressed: () => Navigator.pop(context),
              icon: Icon(
                Icons.close,
                color: Colors.white,
              ),
              label: Text('Close')),
        ),
      ],
    );
  }

  _showDialog(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      //this right here
      child: Container(
        height: 300.0,
        width: 300.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(15.0),
              child: Text(
                'Awesome',
                style: TextStyle(color: Colors.red),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 50.0)),
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Got It!',
                  style: TextStyle(color: Colors.purple, fontSize: 18.0),
                ))
          ],
        ),
      ),
    );
  }

  onTabTapped(int index) {
    onLoadData(listTitle[index]);
  }

  onLoadData(Category cate) async {
    var listTemp = await getAllProductByCategory(cate);

    setState(() {
      list = listTemp;
    });
  }

  onSearchData(String name) async {
    var listTemp = await getAllProductBySearch(name);

    setState(() {
      list = listTemp;
    });
  }

  @protected
  Future<bool> loadWidget(BuildContext context, bool isInit) async {
    debugPrint("CustomState(${T.toString()}).loadWidget executed " +
        (isInit ? "for the first time" : "again"));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return new FutureBuilder(
        future: loadWidget(context, _isInit),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data) {
              _isInit = false;
              return myBuild(context);
            }
          } else {
            return loadingBuild(context);
          }
        });
  }

  @protected
  Widget loadingBuild(BuildContext context) {
    return new Center(
      child: new Container(
        height: 300.0,
        width: 300.0,
        child: new Column(
          children: [
            new CircularProgressIndicator(),
            new Text("Loading"),
          ],
        ),
      ),
    );
    //  new Future.delayed(new Duration(seconds: 3), () {
    //    Navigator.pop(context); //pop dialog
    //   });
  }

  @protected
  Widget myBuild(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        body: new Column(children: <Widget>[
          new Align(
            alignment: Alignment.centerRight,
            child: new Padding(
              padding: EdgeInsets.all(8.0),
              child: new Container(
                  height: 20,
                  width: 240,
                  child: TextField(
                    style: new TextStyle(
                        fontSize: 16.0, height: 1.0, color: Colors.black),
                    decoration: InputDecoration(
                      contentPadding: new EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 20.0),

                        prefixIcon: Icon(Icons.search),
                        hintText: 'Search products',
                        border: const OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(30.0),
                            ))),
                    onChanged: (text) {
                      if (text.length > 1) {
                        return onSearchData(text);
                      }
                      print("Serch Product : $text");
                    },
                  )),
            ),
          ),
          new Expanded(
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child:
              new GridView.count(
              crossAxisCount: widget.foodGridCount,
                padding: const EdgeInsets.all(8.0),
              childAspectRatio: (1 / 1.3),
              controller: new ScrollController(keepScrollOffset: false),
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
                children: list == null ?  new Container() :  list.map<Widget>((Product product) {
                    return getItemUI2(product);
                  }).toList(),
              ),
//              new StaggeredGridView.countBuilder(
//                  crossAxisCount: widget.foodGridCount,
//                  itemCount: list == null ? 0 : list.length,
//                  staggeredTileBuilder: (int index) => new StaggeredTile.count(
//                      // index == 0 ? 2 : 1, index == 0 ? 2.4 : 1.2),
//                      1,
//                      1.3),
//                  mainAxisSpacing: 32.0,
//                  crossAxisSpacing: 32.0,
//                  itemBuilder: (BuildContext context, int index) {
//                    return getItemUI(context, index);
//                  }),
            ),
          ),

          new Align(
            alignment: FractionalOffset.bottomRight,
            child: new Padding(
              padding: EdgeInsets.only(right: 16, bottom: 16),
              child: new Container(
                height: 55,
                child: new Padding(
                  padding: EdgeInsets.all(4),
                  child: new Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      generateButton(0),generateButton(1),generateButton(2),generateButton(3),
                    ],
                  ),
                ),
              ),
            ),
          )


//          new Container(
//            height: 50,
//            decoration: new BoxDecoration(color: Colors.transparent),
//            child: CustomTabBar(
//
//              onTap: onTabTapped,
//              labelColor: Colors.white,
//              unselectedLabelColor: Colors.green,
//              indicator: CustomTabIndicator(),
//              tabs: [
//                Tab(text: getCategoryTitle(listTitle[0])),
//                Tab(
//                  text: getCategoryTitle(listTitle[1]),
//                ),
//                Tab(
//                  text: getCategoryTitle(listTitle[2]),
//                ),
//                Tab(
//                  text: getCategoryTitle(listTitle[3]),
//                ),
//              ],
//            ),
//          ),
        ]),
      ),
    );
  }

  generateButton(index){
    return new RaisedButton(

      onPressed: () {
        onTabTapped(index);
      },
      color: Colors.white,
      padding: EdgeInsets.only(top : 4.0 , bottom : 4.0, left : 24 , right : 24),
      child: Center(
        // Replace with a Row for horizontal icon + text
        child: Text(getCategoryTitle(listTitle[index]),
            style: new TextStyle(color: Colors.red)),
      ),
    );
  }

  getItemUI(context, index) {
    return new Material(
      elevation: 0,
      child: new InkWell(
        onTap: () async {
          if (widget.tableNo != -1) {
            await insetNewOrderItemByTableNo(
                widget.tableNo, list[index].id, "");

            widget.streamController
                .add(MyState.changeText(widget.tableNo, widget.foodGridCount));

          }
//                            showDialog(
//                                context: context,
//                                builder: (context) =>
//                                    _showDialog(context)); // Call the Dialog.
          // showDialog(context: context,builder: (context) => _onTapImage(context,listImg[index])); // Call the Dialog.
        },
        child: Stack(children: <Widget>[
          new Material(
            elevation: 4,
            child: new AspectRatio(
                aspectRatio: 1 / 1,
                child: new Image.asset(
                  "assets/" + list[index].productImage,
                  fit: BoxFit.fill,
                )),
          ),
          new Align(
              alignment: FractionalOffset.bottomCenter,
              child: new Padding(
                padding: EdgeInsets.only(top: 8),
                child: new Container(
                  height: 50,
                  child: new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Expanded(
                            child: Text(
                          list[index].description,
                          style: TextStyle(
                            color: Colors.black87,
                            fontSize: 18,
                            decorationStyle: TextDecorationStyle.wavy,
                          ),
                        )),
                        Text(
                          list[index].price.toStringAsFixed(2),
                          style: TextStyle(
                            color: Colors.green,
                            fontSize: 18,
                            decorationStyle: TextDecorationStyle.wavy,
                          ),
                        )
                        // overflow menu
//                              new Container(
//                                width: 30,
//                                height: 30,
//                                child: PopupMenuButton<Choice>(
//                                  onSelected: _select,
//                                  itemBuilder: (BuildContext context) {
//                                    return choices.skip(1).map((Choice choice) {
//                                      return PopupMenuItem<Choice>(
//                                        value: choice,
//                                        child: Text(choice.title),
//                                      );
//                                    }).toList();
//                                  },
//                                ),
//                              ),
                      ]),
                ),
              )),
        ]),
      ),
    );
  }

  getItemUI2(Product product) {
    return new Material(
      elevation: 0,
      child: new InkWell(
        onTap: () async {
          if (widget.tableNo != -1) {
            await insetNewOrderItemByTableNo(
                widget.tableNo, product.id, "");

            widget.streamController
                .add(MyState.changeText(widget.tableNo, widget.foodGridCount));

          }},
        child: Stack(children: <Widget>[

        new Padding(
        padding: EdgeInsets.all( 8),
          child: new AspectRatio(
                aspectRatio: 1 / 1,
                child: new Image.asset(
                  "assets/" + product.productImage,
                  fit: BoxFit.fill,
                ))),

          new Align(
              alignment: FractionalOffset.bottomCenter,
              child: new Padding(
                padding: EdgeInsets.all( 8),
                child: new Container(
                  height: widget.foodGridCount != 5 ? 40: 50,
                  child: new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Expanded(
                            child: Text(
                              product.description,
                              style: TextStyle(
                                color: Colors.black87,
                                fontSize: 16,
                                decorationStyle: TextDecorationStyle.wavy,
                              ),
                            )),
                        Text(
                          product.price.toStringAsFixed(2),
                          style: TextStyle(
                            color: Colors.green,
                            fontSize: 16,
                            decorationStyle: TextDecorationStyle.wavy,
                          ),
                        )
                      ]),
                ),
              )),
        ]),
      ),
    );
  }

}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'hmmm', icon: Icons.directions_car),
  const Choice(title: 'buy buy buy!', icon: Icons.directions_bike),
  const Choice(title: 'yooooo', icon: Icons.directions_boat),
];

class ChoiceCard extends StatelessWidget {
  const ChoiceCard({Key key, this.choice}) : super(key: key);

  final Choice choice;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.display1;
    return Card(
      color: Colors.white,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(choice.icon, size: 128.0, color: textStyle.color),
            Text(choice.title, style: textStyle),
          ],
        ),
      ),
    );
  }
}

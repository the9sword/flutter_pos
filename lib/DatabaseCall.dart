import 'package:flutter/material.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:path/path.dart' as path;
import 'package:pos/Dao/Order.dart';
import 'package:pos/Dao/OrderItem.dart';
import 'package:pos/Dao/Product.dart';
import 'package:pos/Dao/ResTable.dart';
import 'package:sqflite/sqflite.dart';

import 'Dao/PosUser.dart';

enum BillType { Order, Payment, History }

String getBillCategoryTitle(BillType obj) {
  switch (obj) {
    case BillType.Order:
      {
        return "Order";
      }
    case BillType.Payment:
      {
        return "Payment";
      }
    case BillType.History:
      {
        return "History";
      }
  }
}
//------------------------USER---------------------------------

userLogin(username, password) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();

    final bean = PosUserBean(_adapter);

    Find find = Sql.find('PosUser')
        .where(eq('username', username) & eq("password", password));

    PosUser user = await bean.findOne(find);

    return user;
  } catch (e) {
    print("errorRetrieving" + e.toString());
  }
}

// END=====================USER+------------------------------
// ------------------------------ ORDER -----------------------------
checkAddedOrderWithoutProcess(int tableNo) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = OrderItemBean(_adapter);
    final resTablebean = ResTableBean(_adapter);
    final orderbean = OrderBean(_adapter);

    Order order;
    ResTable table;

    print("READYt : ");
    if (tableNo > 0) {
      Find find = Sql.find('ResTables').where(eq('number', tableNo));

      table = await resTablebean.findOne(find);

      print("table hId : " + table.id.toString());
      Find find1 = Sql.find('pos_order')
          .where(eq('table_id', table.id))
          .and(eq('status', OrderStatus.Order.index));
      order = await orderbean.findOne(find1);
    } else {
      print("READYt okkkk: ");
      Find find1 = Sql.find('pos_order')
          .where(eq('table_id', tableNo))
          .and(eq('status', OrderStatus.Order.index));
      order = await orderbean.findOne(find1);
    }
    if (order == null) {
      return true;
    }
    Find find2 = Sql.find('OrderItem').where(eq('order_id', order.id) &
        IntField('status').eq(OrderStatus.Order.index));
    // .where(eq('order_id', order.id));

    List<OrderItem> list = await bean.findMany(find2);
    if (list == null || list.length == 0) {
      return true;
      print("GOOD to GO " + list.length.toString());
    } else {
      return false;
      print("please save !!!!");
    }

    return list;
  } catch (e) {
    print("errorRetrieving" + e.toString());
  }
}

checkCartIsEmptyProcess(int tableNo) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = OrderItemBean(_adapter);
    final resTablebean = ResTableBean(_adapter);
    final orderbean = OrderBean(_adapter);

    Order order;
    ResTable table;

    print("READYt : ");
    if (tableNo > 0) {
      Find find = Sql.find('ResTables').where(eq('number', tableNo));

      table = await resTablebean.findOne(find);

      print("table hId : " + table.id.toString());
      Find find1 = Sql.find('pos_order')
          .where(eq('table_id', table.id))
          .and(eq('status', OrderStatus.Order.index));
      order = await orderbean.findOne(find1);
    } else {
      print("READYt okkkk: ");
      Find find1 = Sql.find('pos_order')
          .where(eq('table_id', tableNo) & eq('status', OrderStatus.Order.index));
      order = await orderbean.findOne(find1);
    }
    if (order == null) {
      print("READYt empty: ");
      return true;
    }

    Find find2 = Sql.find('OrderItem').where(eq('order_id', order.id) &
    IntField('status').ne(OrderStatus.Deleted.index));
    // .where(eq('order_id', order.id));

    List<OrderItem> list = await bean.findMany(find2);
    if (list == null || list.length == 0) {
      print("READYt empty:2 ");
      return true;
    } else {
      return false;
      print("READYt not empty: ");
      print("please save !!!!");
    }

    return list;
  } catch (e) {
    print("errorRetrieving" + e.toString());
  }
}

getOrderByTableNo(int tableNo) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = OrderBean(_adapter);
    Find find = Sql.find('pos_order').where(eq('table_id', tableNo));
    // .where(IntField('likes').eq(10) & IntField('replies').eq(5))
    //.where(eq('author', 'teja') | like('author', 'kleak*'))
    // .orderBy('author', true)
    // .limit(10)
    // .groupByMany(['message', 'likes'])
    ;

    List<Order> list = await bean.findMany(find);
    print(list.length);
    // print(list[0].description);

    // _adapter.close();
    return list;
  } catch (e) {
    print("errorRetrieving" + e.toString());
  }
}

getOrderByStatus(OrderStatus status) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = OrderBean(_adapter);
    Find find = Sql.find('pos_order').where(eq('status', status));
    // .where(IntField('likes').eq(10) & IntField('replies').eq(5))
    //.where(eq('author', 'teja') | like('author', 'kleak*'))
    // .orderBy('author', true)
    // .limit(10)
    // .groupByMany(['message', 'likes'])
    ;

    List<Order> list = await bean.findMany(find);
    print(list.length);
    // print(list[0].description);

    // _adapter.close();
    return list;
  } catch (e) {
    print("errorRetrieving" + e.toString());
  }
}

getOrderItemListByOrderId(int orderId) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = OrderItemBean(_adapter);
    Find find = Sql.find('OrderItem').where(eq('order_id', orderId));
    // .where(IntField('likes').eq(10) & IntField('replies').eq(5))
    //.where(eq('author', 'teja') | like('author', 'kleak*'))
    // .orderBy('author', true)
    // .limit(10)
    // .groupByMany(['message', 'likes'])
    ;

    List<OrderItem> list = await bean.findMany(find);
    print(list.length);
    // print(list[0].description);

    // _adapter.close();
    return list;
  } catch (e) {
    print("errorRetrieving" + e.toString());
  }
}

getOrderItemListByTableNo(int tableNo) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = OrderItemBean(_adapter);
    final resTablebean = ResTableBean(_adapter);
    final orderbean = OrderBean(_adapter);

    Order order;
    ResTable table;

    print("READYt : ");
    if (tableNo > 0) {
      Find find = Sql.find('ResTables').where(eq('number', tableNo));

      table = await resTablebean.findOne(find);

      print("table hId : " + table.id.toString());
      Find find1 = Sql.find('pos_order')
          .where(eq('table_id', table.id) & eq('status', OrderStatus.Order.index));
      order = await orderbean.findOne(find1);
    } else {
      print("READYt okkkk: ");
      Find find1 = Sql.find('pos_order')
          .where(eq('table_id', tableNo) & eq('status', OrderStatus.Order.index));

      order = await orderbean.findOne(find1);
      print("ORDER = "+ order.status.toString());
    }

    Find find2 = Sql.find('OrderItem').where(eq('order_id', order.id) &
        IntField('status').ne(OrderStatus.Deleted.index));
    // .where(eq('order_id', order.id));

    List<OrderItem> list = await bean.findMany(find2);
    print(list.length);

    return list;
  } catch (e) {
    print("errorRetrieving" + e.toString());
  }
}

getActiveOrderByTableNo(int tableNo) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();

    final bean = OrderItemBean(_adapter);
    final resTablebean = ResTableBean(_adapter);
    final orderbean = OrderBean(_adapter);

    Order order;
    ResTable table;

    print("READYt : ");
    if (tableNo > 0) {
      Find find = Sql.find('ResTables').where(eq('number', tableNo));

      table = await resTablebean.findOne(find);

      print("table hId : " + table.id.toString());
      Find find1 = Sql.find('pos_order')
          .where(eq('table_id', table.id) & ltEqInt('status', OrderStatus.Hold.index));
      order = await orderbean.findOne(find1);
    } else {
      print("READYt okkkk: ");
      Find find1 = Sql.find('pos_order')
          .where(eq('table_id', tableNo) & ltEqInt('status', OrderStatus.Hold.index));

      order = await orderbean.findOne(find1);
      print("ORDER = "+ order.status.toString());
    }


//    Find find2 = Sql.find('pos_order')
//        // .where(eq('tableId', table.id))
//        .where(IntField('table_id').eq(table.id) &
//            (IntField('status').eq(OrderStatus.Order.index)));
//    //.and(eq('status', OrderStatus.Order));
//
//    // .where(IntField('likes').eq(10) & IntField('replies').eq(5))
//    //.where(eq('author', 'teja') | like('author', 'kleak*'))
//    // .orderBy('author', true)
//    // .limit(10)
//    // .groupByMany(['message', 'likes'])



    return order;
  } catch (e) {
    print("errorRetrieving" + e.toString());
  }
}

checkTableOrderStatus(int TableNo) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = OrderBean(_adapter);
    Find find = Sql.find('pos_order').where(eq('tableId', TableNo));
    // .where(IntField('likes').eq(10) & IntField('replies').eq(5))
    //.where(eq('author', 'teja') | like('author', 'kleak*'))
    // .orderBy('author', true)
    // .limit(10)
    // .groupByMany(['message', 'likes'])

    Order order = await bean.findOne(find);

    return order.status;
  } catch (e) {
    print("errorRetrieving" + e.toString());
  }
}

insertNewOrder(int TableNo) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = OrderBean(_adapter);
    await bean.insert(new Order.make(
        null, "", TableNo, null, OrderStatus.Order.index, DateTime.now()));
  } catch (e) {
    print("errorRetrieving" + e.toString());
  }
}

insetNewOrderItemByTableNo(int tableNo, int productId, String remarks) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();

    final orderbean = OrderBean(_adapter);
    final orderItembean = OrderItemBean(_adapter);
    final resTablebean = ResTableBean(_adapter);
    Order order;
    ResTable table;

    print("READYt : ");
    if (tableNo > 0) {
      Find find = Sql.find('ResTables').where(eq('number', tableNo));

      table = await resTablebean.findOne(find);

      print("table hId : " + table.id.toString());
      Find find1 = Sql.find('pos_order')
          .where(eq('table_id', table.id))
          .and(ltEqInt('status', OrderStatus.Hold.index));
      order = await orderbean.findOne(find1);
    } else {
      print("READYt okkkk: ");
      Find find1 = Sql.find('pos_order')
          .where(eq('table_id', tableNo))
          .and(ltEqInt('status', OrderStatus.Hold.index));
      order = await orderbean.findOne(find1);
    }

    if (order != null) {
      print("orderId got : " + order.tableId.toString());
      await orderItembean.insert(new OrderItem.make(null, order.id, productId,
          remarks, 1, 0, OrderStatus.Order.index, DateTime.now()));
    } else {
      int orderId = await orderbean.insert(new Order.make(
          null,
          "new order",
          (table == null ? tableNo : table.id),
          null,
          OrderStatus.Order.index,
          DateTime.now()));
      print("orderId new: " + orderId.toString());
      await orderItembean.insert(new OrderItem.make(null, orderId, productId,
          remarks, 1, 0, OrderStatus.Order.index, DateTime.now()));
    }
  } catch (e) {
    print("haha" + e.toString());
  }
}

insetNewOrderItem(int orderId, int productId, String remarks) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    // final OrderBea = OrderBean(_adapter);
    // final bean = ProductBean(_adapter);
    final bean = OrderItemBean(_adapter);
    // Find find = Sql.find('Order').where(eq('tableId' , TableNo)).and(eq('status' , OrderStatus.Ordered));

    await bean.insert(new OrderItem.make(null, orderId, productId, remarks, 1,
        0, OrderStatus.Order.index, DateTime.now()));
  } catch (e) {
    print("haha" + e.toString());
  }
}

updateOrder(int orderId) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = OrderBean(_adapter);
    Order order = await bean.find(orderId);
    order.remarks = 'teja hackborn';
    await bean.update(order);
  } catch (e) {
    print("haha" + e.toString());
  }
}

deleteOrder(order) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = OrderBean(_adapter);

    if (order != null) {
      order.status = OrderStatus.Deleted.index;
      await bean.update(order);
    }
  } catch (e) {
    print("haha" + e.toString());
  }
}

deleteOrderByOrderId(orderId) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = OrderBean(_adapter);

    Find find2 = Sql.find('pos_order')
        // .where(eq('tableId', table.id))
        .where(IntField('id').eq(orderId));

    Order order = await bean.findOne(orderId);
    if (order != null) {
      order.status = OrderStatus.Deleted.index;
      await bean.update(order);
    }
  } catch (e) {
    print("haha" + e.toString());
  }
}

updateOrderItem(int orderItemId) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    // final OrderBea = OrderBean(_adapter);
    // final bean = ProductBean(_adapter);
    final bean = OrderItemBean(_adapter);
    // Find find = Sql.find('Order').where(eq('tableId' , TableNo)).and(eq('status' , OrderStatus.Ordered));
    OrderItem orderItem = await bean.find(orderItemId);
    orderItem.remarks = 'teja hackborn';
    await bean.update(orderItem);
  } catch (e) {
    print("haha" + e.toString());
  }
}

updateOrderItemToConfirm(List<OrderItem> orderItems , orderStatus) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();

    final bean = OrderItemBean(_adapter);

    for (var i = 0; i < orderItems.length; i++) {
      if (orderItems[i].status < OrderStatus.OrderConfirm.index)
        orderItems[i].status = orderStatus.index;
    }
    await bean.updateMany(orderItems);
  } catch (e) {
    print("haha" + e.toString());
  }
}

deleteOrderItem(OrderItem orderItem) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = OrderItemBean(_adapter);

    orderItem.status = OrderStatus.Deleted.index;

    await bean.update(orderItem);
  } catch (e) {
    print("haha" + e.toString());
  }
}
// ----------------------------- END ORDER --------------------------

//-------------------------- PRODUCT --------------------------------
getAllProductBySearch(String name) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = ProductBean(_adapter);

    Find find = Sql.find('Product').where(
            StrField('description').like('%$name%') |
                StrField("id").like('%$name%'))
        //.where(eq('author', 'teja') | like('author', 'kleak*'))
        // .orderBy('author', true)
        // .limit(10)
        // .groupByMany(['message', 'likes'])
        ;

    List<Product> list = await bean.findMany(find);
    print(list.length);
    // print(list[0].description);

    // _adapter.close();
    return list;
  } catch (e) {
    print("error " + e.toString());
  }
}

getAllProductByCategory(Category category) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = ProductBean(_adapter);
    print("aaaa");
    Find find = Sql.find('Product').where(eq('category', category.index))
        // .where(IntField('likes').eq(10) & IntField('replies').eq(5))
        //.where(eq('author', 'teja') | like('author', 'kleak*'))
        // .orderBy('author', true)
        // .limit(10)
        // .groupByMany(['message', 'likes'])
        ;

    List<Product> list = await bean.findMany(find);
    print(list.length);
    // print(list[0].description);

    //  _adapter.close();
    return list;
  } catch (e) {
    print("haha" + e.toString());
  }
}

Future<double> getProductPriceByProductId(int productId) async {
  //await Future.delayed(Duration(milliseconds: 500));

  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = ProductBean(_adapter);

    Find find = Sql.find('Product').where(eq('id', productId));
    Product product = await bean.findOne(find);

    return product.price;
  } catch (e) {
    print("haha" + e.toString());
  }
}

getProductByProductId(int productId) async {
  //await Future.delayed(Duration(milliseconds: 500));

  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = ProductBean(_adapter);

    Find find = Sql.find('Product').where(eq('id', productId));
    Product product = await bean.findOne(find);

    return {
      'img': product.productImage,
      'name': product.description,
      'price': product.price.toStringAsFixed(2)
    };
  } catch (e) {
    print("haha" + e.toString());
  }
}
//--------------------------END PRODUCT -------------------------

// -------------------------RESTABLE ----------------------------
getAllTable() async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = ResTableBean(_adapter);

    List<ResTable> list = await bean.getAll();

    return list;
  } catch (e) {
    print("error " + e.toString());
  }
}

getTableOrderStatus(int tableId) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();

    final bean = OrderBean(_adapter);
    Find find = Sql.find('pos_order')
        .where(eq('table_id', tableId) & ltInt("status", OrderStatus.Deleted.index));

    Order order = await bean.findOne(find);

    return {'isOrdered': order != null ? true : false};
  } catch (e) {
    print("error " + e.toString());
  }
}
//--------------------------END RESTABLE -------------------------

getAllBillByCategory(BillType obj) async {
  SqfliteAdapter _adapter;
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();
    final bean = ProductBean(_adapter);
    print("aaaa");
    Find find = Sql.find('Product').where(eq('category', obj.index))
        // .where(IntField('likes').eq(10) & IntField('replies').eq(5))
        //.where(eq('author', 'teja') | like('author', 'kleak*'))
        // .orderBy('author', true)
        // .limit(10)
        // .groupByMany(['message', 'likes'])
        ;

    List<Product> list = await bean.findMany(find);
    print(list.length);
    // print(list[0].description);

    // _adapter.close();
    return list;
  } catch (e) {
    print("haha" + e.toString());
  }
}

class CustomTabIndicator extends Decoration {
  @override
  _CustomPainter createBoxPainter([VoidCallback onChanged]) {
    return new _CustomPainter(this, onChanged);
  }
}

class _CustomPainter extends BoxPainter {
  final CustomTabIndicator decoration;
  final double indicatorHeight = 25;

  _CustomPainter(this.decoration, VoidCallback onChanged)
      : assert(decoration != null),
        super(onChanged);

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    assert(configuration != null);
    assert(configuration.size != null);

    //offset is the position from where the decoration should be drawn.
    //configuration.size tells us about the height and width of the tab.
    final Rect rect = Offset(offset.dx + 10,
            (configuration.size.height / 2) - indicatorHeight / 2) &
        Size(configuration.size.width - 20, indicatorHeight);

    //final Rect rect = offset & configuration.size;
    final Paint paint = Paint();
    // paint.color = Color(0xffffff00);
    paint.color = Colors.green.withOpacity(0.8);
    paint.style = PaintingStyle.fill;
    canvas.drawRRect(
        RRect.fromRectAndRadius(rect, Radius.circular(15.0)), paint);
  }
}

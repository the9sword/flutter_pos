import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pos/Home.dart';
import 'package:pos/MyState.dart';

import 'GridFragment.dart';
import 'TableFragment2.dart';

class FirstFragment extends StatefulWidget {
  final homeObservable observable;
  final StreamController<MyState> streamController;
  final StreamController<int> _onActionBarTab;
  final StreamController<int> onOrderChangeController;
  final controller = new PageController();
  int foodGridCount;
  int currentIndex = 0;

  FirstFragment(this.observable, this.streamController, this._onActionBarTab,
      this.onOrderChangeController, this.foodGridCount,
      {Key key})
      : super(key: key);

  _FirstFragmentState state;

  @override
  _FirstFragmentState createState() {
    _onActionBarTab.stream.listen((data) {
      if (data == 1) {
        //controller.animateToPage(data , duration: Duration(milliseconds: 300) , curve: Curves.ease);
        state.onTabTapped(1);
      } else {
        state.onTabTapped(0);
      }
      print("Task Donbbbbbbbe1");
    }, onDone: () {
      print("Task Done1");
    }, onError: (error) {
      print("Some Error1");
    });

    return state = _FirstFragmentState();
  }
}

class _FirstFragmentState extends State<FirstFragment>
    with SingleTickerProviderStateMixin {
  //TabController _controller;

  StreamController tableStreamController;

  List<Widget> list;
  BottomNavigationBarItem item = BottomNavigationBarItem(
      icon: Icon(Icons.restaurant), title: Text('Table'));

  wrapMaterialCard(widget) {
    return new Padding(
        //padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 71),
        padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
        child: new Material(
          elevation: 6,
          child: widget,
        ));
  }

  @override
  void initState() {
    super.initState();

    tableStreamController = new StreamController.broadcast();
    list = [
      wrapMaterialCard(
          new GridFragment(widget.streamController, widget.foodGridCount)),
      wrapMaterialCard(
          new TableFragment2(tableStreamController, widget.observable)),
    ];

    tableStreamController.stream.listen((data) {
      onTabTapped(0);
      print("DataReceived Order: " + data.toString());
    }, onDone: () {
      print("Task Done");
    }, onError: (error) {
      print("Some Error");
    });
  }

  void onTabTapped(int index) {
    widget.currentIndex = index;
    setState(() {});
  }

  onButtonOrder(int) {
    widget.onOrderChangeController.add(int);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Stack(children: <Widget>[
      //PageView(controller : widget.controller ,children: list),
      new Padding(
        padding: EdgeInsets.only(bottom: 76),
        child: new Container(
            width: double.infinity,
            height: double.infinity,
            child: list[widget.currentIndex]),
      ),
      new Align(
        alignment: FractionalOffset.bottomRight,
        child: new Padding(
          padding: EdgeInsets.only(right: 16, bottom: 16),
          child: new Container(
            height: 55,
            child: new Padding(
              padding: EdgeInsets.all(4),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new OutlineButton(
                    borderSide: BorderSide(color: Colors.red),
                    onPressed: () {
                      onButtonOrder(3);
                    },
                    color: Colors.red,
                    padding: EdgeInsets.all(4.0),
                    child: Center(
                      // Replace with a Row for horizontal icon + text
                      child: Text("CANCEL\nORDER",
                          style: new TextStyle(color: Colors.red)),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(left :16 , right:16),
                    child: new OutlineButton(
                      borderSide: BorderSide(color: Colors.lightGreen),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(2.0)),
                      onPressed: () {
                        onButtonOrder(2);
                      },
                      color: Colors.white,
                      padding: EdgeInsets.all(4.0),
                      child: Center(
                        // Replace with a Row for horizontal icon + text
                        child: Text("HOLD\nORDER",
                            style: new TextStyle(color: Colors.lightGreen)),
                      ),
                    ),
                  ),
                  new OutlineButton(
                    onPressed: () {
                      onButtonOrder(1);
                    },
                    //color: Colors.greenAccent,
                    borderSide: BorderSide(color: Colors.green),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(2.0)),
                    padding: EdgeInsets.all(4.0),
                    child: Center(
                      // Replace with a Row for horizontal icon + text
                      child: Text("PLACE\nORDER",
                          style: new TextStyle(color: Colors.green)),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      )
    ]

//      new TabBarView(
//        controller: _controller,
//        children: list,
//      ),
//      bottomNavigationBar: BottomNavigationBar(
//        type: BottomNavigationBarType.fixed,
//        onTap: onTabTapped, // new
//        currentIndex: widget.currentIndex, // new
//
//        items: [
//          BottomNavigationBarItem(
//            icon: new Icon(Icons.home),
//            title: new Text('Home'),
//          ),
//          BottomNavigationBarItem(
//            icon: new Icon(Icons.local_dining),
//            title: new Text('Food'),
//          ),
//          item,
//          BottomNavigationBarItem(
//              icon: Icon(Icons.person), title: Text('Profile'))
//        ],
//      ),
        );
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'LoyaltyUser.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _LoyaltyUserBean implements Bean<LoyaltyUser> {
  final id = IntField('id');
  final name = StrField('name');
  final role = StrField('role');
  final remarks = StrField('remarks');
  final dateCreated = DateTimeField('date_created');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        id.name: id,
        name.name: name,
        role.name: role,
        remarks.name: remarks,
        dateCreated.name: dateCreated,
      };
  LoyaltyUser fromMap(Map map) {
    LoyaltyUser model = LoyaltyUser();
    model.id = adapter.parseValue(map['id']);
    model.name = adapter.parseValue(map['name']);
    model.role = adapter.parseValue(map['role']);
    model.remarks = adapter.parseValue(map['remarks']);
    model.dateCreated = adapter.parseValue(map['date_created']);

    return model;
  }

  List<SetColumn> toSetColumns(LoyaltyUser model,
      {bool update = false, Set<String> only, bool onlyNonNull: false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      ret.add(id.set(model.id));
      ret.add(name.set(model.name));
      ret.add(role.set(model.role));
      ret.add(remarks.set(model.remarks));
      ret.add(dateCreated.set(model.dateCreated));
    } else if (only != null) {
      if (only.contains(id.name)) ret.add(id.set(model.id));
      if (only.contains(name.name)) ret.add(name.set(model.name));
      if (only.contains(role.name)) ret.add(role.set(model.role));
      if (only.contains(remarks.name)) ret.add(remarks.set(model.remarks));
      if (only.contains(dateCreated.name))
        ret.add(dateCreated.set(model.dateCreated));
    } else /* if (onlyNonNull) */ {
      if (model.id != null) {
        ret.add(id.set(model.id));
      }
      if (model.name != null) {
        ret.add(name.set(model.name));
      }
      if (model.role != null) {
        ret.add(role.set(model.role));
      }
      if (model.remarks != null) {
        ret.add(remarks.set(model.remarks));
      }
      if (model.dateCreated != null) {
        ret.add(dateCreated.set(model.dateCreated));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists: false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(id.name, primary: true, isNullable: false);
    st.addStr(name.name, isNullable: false);
    st.addStr(role.name, isNullable: false);
    st.addStr(remarks.name, isNullable: true);
    st.addDateTime(dateCreated.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(LoyaltyUser model,
      {bool cascade: false, bool onlyNonNull: false, Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.insert(insert);
  }

  Future<void> insertMany(List<LoyaltyUser> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(LoyaltyUser model,
      {bool cascade: false, Set<String> only, bool onlyNonNull: false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.upsert(upsert);
  }

  Future<void> upsertMany(List<LoyaltyUser> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(LoyaltyUser model,
      {bool cascade: false,
      bool associate: false,
      Set<String> only,
      bool onlyNonNull: false}) async {
    final Update update = updater
        .where(this.id.eq(model.id))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<LoyaltyUser> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.id.eq(model.id));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<LoyaltyUser> find(int id,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.id.eq(id));
    return await findOne(find);
  }

  Future<int> remove(int id) async {
    final Remove remove = remover.where(this.id.eq(id));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<LoyaltyUser> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.id.eq(model.id));
    }
    return adapter.remove(remove);
  }
}

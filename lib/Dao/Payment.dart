import 'dart:async';
import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'Payment.jorm.dart';

// The model
class Payment {
  Payment();

  Payment.make(this.id, this.orderId, this.paymentType, this.remarks, this.status, this.dateCreated);

  @PrimaryKey(auto: true)
  int id;

  @Column(isNullable: false)
  int orderId;

  @Column(isNullable: false)
  int paymentType;

  @Column(isNullable: true)
  String remarks;

  @Column(isNullable: false)
  int status;

  @Column(isNullable: true)
  String loyaltyUser;

  @Column(isNullable: false)
  DateTime dateCreated;


}

@GenBean()
class PaymentBean extends Bean<Payment> with _PaymentBean {
  PaymentBean(Adapter adapter) : super(adapter);


  final String tableName = 'Payment';
}
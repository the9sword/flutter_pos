// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Product.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _ProductBean implements Bean<Product> {
  final id = IntField('id');
  final description = StrField('description');
  final category = IntField('category');
  final remarks = StrField('remarks');
  final productImage = StrField('product_image');
  final price = DoubleField('price');
  final available = BoolField('available');
  final availableStartDate = DateTimeField('available_start_date');
  final availableEndDate = DateTimeField('available_end_date');
  final dateCreated = DateTimeField('date_created');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        id.name: id,
        description.name: description,
        category.name: category,
        remarks.name: remarks,
        productImage.name: productImage,
        price.name: price,
        available.name: available,
        availableStartDate.name: availableStartDate,
        availableEndDate.name: availableEndDate,
        dateCreated.name: dateCreated,
      };
  Product fromMap(Map map) {
    Product model = Product();
    model.id = adapter.parseValue(map['id']);
    model.description = adapter.parseValue(map['description']);
    model.category = adapter.parseValue(map['category']);
    model.remarks = adapter.parseValue(map['remarks']);
    model.productImage = adapter.parseValue(map['product_image']);
    model.price = adapter.parseValue(map['price']);
    model.available = adapter.parseValue(map['available']);
    model.availableStartDate = adapter.parseValue(map['available_start_date']);
    model.availableEndDate = adapter.parseValue(map['available_end_date']);
    model.dateCreated = adapter.parseValue(map['date_created']);

    return model;
  }

  List<SetColumn> toSetColumns(Product model,
      {bool update = false, Set<String> only, bool onlyNonNull: false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      ret.add(id.set(model.id));
      ret.add(description.set(model.description));
      ret.add(category.set(model.category));
      ret.add(remarks.set(model.remarks));
      ret.add(productImage.set(model.productImage));
      ret.add(price.set(model.price));
      ret.add(available.set(model.available));
      ret.add(availableStartDate.set(model.availableStartDate));
      ret.add(availableEndDate.set(model.availableEndDate));
      ret.add(dateCreated.set(model.dateCreated));
    } else if (only != null) {
      if (only.contains(id.name)) ret.add(id.set(model.id));
      if (only.contains(description.name))
        ret.add(description.set(model.description));
      if (only.contains(category.name)) ret.add(category.set(model.category));
      if (only.contains(remarks.name)) ret.add(remarks.set(model.remarks));
      if (only.contains(productImage.name))
        ret.add(productImage.set(model.productImage));
      if (only.contains(price.name)) ret.add(price.set(model.price));
      if (only.contains(available.name))
        ret.add(available.set(model.available));
      if (only.contains(availableStartDate.name))
        ret.add(availableStartDate.set(model.availableStartDate));
      if (only.contains(availableEndDate.name))
        ret.add(availableEndDate.set(model.availableEndDate));
      if (only.contains(dateCreated.name))
        ret.add(dateCreated.set(model.dateCreated));
    } else /* if (onlyNonNull) */ {
      if (model.id != null) {
        ret.add(id.set(model.id));
      }
      if (model.description != null) {
        ret.add(description.set(model.description));
      }
      if (model.category != null) {
        ret.add(category.set(model.category));
      }
      if (model.remarks != null) {
        ret.add(remarks.set(model.remarks));
      }
      if (model.productImage != null) {
        ret.add(productImage.set(model.productImage));
      }
      if (model.price != null) {
        ret.add(price.set(model.price));
      }
      if (model.available != null) {
        ret.add(available.set(model.available));
      }
      if (model.availableStartDate != null) {
        ret.add(availableStartDate.set(model.availableStartDate));
      }
      if (model.availableEndDate != null) {
        ret.add(availableEndDate.set(model.availableEndDate));
      }
      if (model.dateCreated != null) {
        ret.add(dateCreated.set(model.dateCreated));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists: false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(id.name, primary: true, isNullable: false);
    st.addStr(description.name, isNullable: false);
    st.addInt(category.name, isNullable: false);
    st.addStr(remarks.name, isNullable: true);
    st.addStr(productImage.name, isNullable: true);
    st.addDouble(price.name, isNullable: false);
    st.addBool(available.name, isNullable: true);
    st.addDateTime(availableStartDate.name, isNullable: true);
    st.addDateTime(availableEndDate.name, isNullable: true);
    st.addDateTime(dateCreated.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Product model,
      {bool cascade: false, bool onlyNonNull: false, Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.insert(insert);
  }

  Future<void> insertMany(List<Product> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Product model,
      {bool cascade: false, Set<String> only, bool onlyNonNull: false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.upsert(upsert);
  }

  Future<void> upsertMany(List<Product> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Product model,
      {bool cascade: false,
      bool associate: false,
      Set<String> only,
      bool onlyNonNull: false}) async {
    final Update update = updater
        .where(this.id.eq(model.id))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Product> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.id.eq(model.id));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Product> find(int id,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.id.eq(id));
    return await findOne(find);
  }

  Future<int> remove(int id) async {
    final Remove remove = remover.where(this.id.eq(id));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Product> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.id.eq(model.id));
    }
    return adapter.remove(remove);
  }
}

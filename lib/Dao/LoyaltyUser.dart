import 'dart:async';
import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'LoyaltyUser.jorm.dart';

// The model
class LoyaltyUser {
  LoyaltyUser();

  LoyaltyUser.make(this.id, this.role, this.name, this.remarks, this.dateCreated);

  @PrimaryKey()
  int id;

  @Column(isNullable: false)
  String name;

  @Column(isNullable: false)
  String role;

  @Column(isNullable: true)
  String remarks;

  @Column(isNullable: false)
  DateTime dateCreated;


}

@GenBean()
class LoyaltyUserBean extends Bean<LoyaltyUser> with _LoyaltyUserBean {
  LoyaltyUserBean(Adapter adapter) : super(adapter);

  Future<int> updateReadField(int id, bool read) async {
    //Update st = updater.where(this.id.eq(id)).set(this.read, read);
    //return adapter.update(st);
  }

  final String tableName = 'User';
}
import 'dart:async';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:jaguar_query/jaguar_query.dart';

part 'Product.jorm.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

enum Category {
  Rice,
  Noodle,
  Dessert,
  Breverage,
  Special,
}

String getCategoryTitle(Category cate) {
  switch (cate) {
    case Category.Rice:
      {
        return "Rice";
      }
    case Category.Noodle:
      {
        return "Noodle";
      }
    case Category.Dessert:
      {
        return "Dessert";
      }
    case Category.Breverage:
      {
        return "Breverage";
      }
    case Category.Special:
      {
        return "Special";
      }
  }
}

// The model
class Product {
  Product();

  Product.make(
      this.id,
      this.description,
      this.category,
      this.remarks,
      this.productImage,
      this.price,
      this.available,
      this.availableStartDate,
      this.availableEndDate,
      this.dateCreated);

  @PrimaryKey()
  int id;

  @Column(isNullable: false)
  String description;

  @Column(isNullable: false)
  int category;


  @Column(isNullable: true)
  String remarks;

  @Column(isNullable: true)
  String productImage;

  @Column(isNullable: false)
  double price;

  @Column(isNullable: true)
  bool available;

  @Column(isNullable: true)
  DateTime availableStartDate;

  @Column(isNullable: true)
  DateTime availableEndDate;

  @Column(isNullable: false)
  DateTime dateCreated;
}

@GenBean()
class ProductBean extends Bean<Product> with _ProductBean {

  ProductBean(Adapter adapter) : super(adapter);

 // Future<int> updateReadField(int id, bool read) async {
    //Update st = updater.where(this.id.eq(id)).set(this.read, read);
    //return adapter.update(st);
 // }

  final String tableName = 'Product';
}

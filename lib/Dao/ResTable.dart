import 'dart:async';
import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'ResTable.jorm.dart';

// The model
class ResTable {
  ResTable();

 // ResTable.make(this.id, this.number, this.available);
  ResTable.make(this.id, this.number, this.available,this.x,this.y);
  @PrimaryKey(auto: true)
  int id;

  @Column(isNullable: false)
  String number;

  @Column(isNullable: false)
  bool available;

  @Column(isNullable: true)
  double x;

  @Column(isNullable: true)
  double y;

}

@GenBean()
class ResTableBean extends Bean<ResTable> with _ResTableBean {
  ResTableBean(Adapter adapter) : super(adapter);

  final String tableName = 'ResTables';
}
import 'dart:async';
import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'OrderItem.jorm.dart';

// The model
class OrderItem {
  OrderItem();

  OrderItem.make(this.id,this.orderId,this.productId,
      this.remarks,this.quantity,this.discount, this.status, this.dateCreated);

  @PrimaryKey(auto: true)
  int id;

  @Column(isNullable: false)
  int orderId;

  @Column(isNullable: false)
  int productId;

  @Column(isNullable: true)
  String remarks;

  @Column(isNullable: false)
  int quantity;

  @Column(isNullable: true)
  double discount;

  @Column(isNullable: true)
  int status;

  @Column(isNullable: false)
  DateTime dateCreated;


}

@GenBean()
class OrderItemBean extends Bean<OrderItem> with _OrderItemBean {
  OrderItemBean(Adapter adapter) : super(adapter);

  Future<int> updateReadField(int id, bool read) async {
    //Update st = updater.where(this.id.eq(id)).set(this.read, read);
    //return adapter.update(st);
  }

  final String tableName = 'OrderItem';
}
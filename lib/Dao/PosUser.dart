import 'dart:async';
import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'PosUser.jorm.dart';

enum UserRole {
  Cashier,
  Manager,
  SuperAdmin,
}

// The model
class PosUser {
  PosUser();

  PosUser.make(this.id, this.role, this.name,this.username,this.password, this.branch, this.remarks, this.dateCreated);

  @PrimaryKey()
  int id;

  @Column(isNullable: false)
  String name;

  @Column(isNullable: false)
  String username;

  @Column(isNullable: false)
  String password;

  @Column(isNullable: false)
  int role;

  @Column(isNullable: false)
  String branch;

  @Column(isNullable: true)
  String remarks;

  @Column(isNullable: false)
  DateTime dateCreated;


}

@GenBean()
class PosUserBean extends Bean<PosUser> with _PosUserBean {
  PosUserBean(Adapter adapter) : super(adapter);

  final String tableName = 'PosUser';
}
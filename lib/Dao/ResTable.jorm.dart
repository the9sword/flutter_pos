// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ResTable.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _ResTableBean implements Bean<ResTable> {
  final id = IntField('id');
  final number = StrField('number');
  final available = BoolField('available');
  final x = DoubleField('x');
  final y = DoubleField('y');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        id.name: id,
        number.name: number,
        available.name: available,
        x.name: x,
        y.name: y,
      };
  ResTable fromMap(Map map) {
    ResTable model = ResTable();
    model.id = adapter.parseValue(map['id']);
    model.number = adapter.parseValue(map['number']);
    model.available = adapter.parseValue(map['available']);
    model.x = adapter.parseValue(map['x']);
    model.y = adapter.parseValue(map['y']);

    return model;
  }

  List<SetColumn> toSetColumns(ResTable model,
      {bool update = false, Set<String> only, bool onlyNonNull: false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.id != null) {
        ret.add(id.set(model.id));
      }
      ret.add(number.set(model.number));
      ret.add(available.set(model.available));
      ret.add(x.set(model.x));
      ret.add(y.set(model.y));
    } else if (only != null) {
      if (model.id != null) {
        if (only.contains(id.name)) ret.add(id.set(model.id));
      }
      if (only.contains(number.name)) ret.add(number.set(model.number));
      if (only.contains(available.name))
        ret.add(available.set(model.available));
      if (only.contains(x.name)) ret.add(x.set(model.x));
      if (only.contains(y.name)) ret.add(y.set(model.y));
    } else /* if (onlyNonNull) */ {
      if (model.id != null) {
        ret.add(id.set(model.id));
      }
      if (model.number != null) {
        ret.add(number.set(model.number));
      }
      if (model.available != null) {
        ret.add(available.set(model.available));
      }
      if (model.x != null) {
        ret.add(x.set(model.x));
      }
      if (model.y != null) {
        ret.add(y.set(model.y));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists: false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(id.name, primary: true, autoIncrement: true, isNullable: false);
    st.addStr(number.name, isNullable: false);
    st.addBool(available.name, isNullable: false);
    st.addDouble(x.name, isNullable: true);
    st.addDouble(y.name, isNullable: true);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(ResTable model,
      {bool cascade: false, bool onlyNonNull: false, Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(id.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      ResTable newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<ResTable> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(ResTable model,
      {bool cascade: false, Set<String> only, bool onlyNonNull: false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(id.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      ResTable newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<ResTable> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(ResTable model,
      {bool cascade: false,
      bool associate: false,
      Set<String> only,
      bool onlyNonNull: false}) async {
    final Update update = updater
        .where(this.id.eq(model.id))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<ResTable> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.id.eq(model.id));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<ResTable> find(int id,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.id.eq(id));
    return await findOne(find);
  }

  Future<int> remove(int id) async {
    final Remove remove = remover.where(this.id.eq(id));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<ResTable> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.id.eq(model.id));
    }
    return adapter.remove(remove);
  }
}

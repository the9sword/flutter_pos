// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Order.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _OrderBean implements Bean<Order> {
  final id = IntField('id');
  final remarks = StrField('remarks');
  final tableId = IntField('table_id');
  final userId = IntField('user_id');
  final status = IntField('status');
  final dateCreated = DateTimeField('date_created');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        id.name: id,
        remarks.name: remarks,
        tableId.name: tableId,
        userId.name: userId,
        status.name: status,
        dateCreated.name: dateCreated,
      };
  Order fromMap(Map map) {
    Order model = Order();
    model.id = adapter.parseValue(map['id']);
    model.remarks = adapter.parseValue(map['remarks']);
    model.tableId = adapter.parseValue(map['table_id']);
    model.userId = adapter.parseValue(map['user_id']);
    model.status = adapter.parseValue(map['status']);
    model.dateCreated = adapter.parseValue(map['date_created']);

    return model;
  }

  List<SetColumn> toSetColumns(Order model,
      {bool update = false, Set<String> only, bool onlyNonNull: false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.id != null) {
        ret.add(id.set(model.id));
      }
      ret.add(remarks.set(model.remarks));
      ret.add(tableId.set(model.tableId));
      ret.add(userId.set(model.userId));
      ret.add(status.set(model.status));
      ret.add(dateCreated.set(model.dateCreated));
    } else if (only != null) {
      if (model.id != null) {
        if (only.contains(id.name)) ret.add(id.set(model.id));
      }
      if (only.contains(remarks.name)) ret.add(remarks.set(model.remarks));
      if (only.contains(tableId.name)) ret.add(tableId.set(model.tableId));
      if (only.contains(userId.name)) ret.add(userId.set(model.userId));
      if (only.contains(status.name)) ret.add(status.set(model.status));
      if (only.contains(dateCreated.name))
        ret.add(dateCreated.set(model.dateCreated));
    } else /* if (onlyNonNull) */ {
      if (model.id != null) {
        ret.add(id.set(model.id));
      }
      if (model.remarks != null) {
        ret.add(remarks.set(model.remarks));
      }
      if (model.tableId != null) {
        ret.add(tableId.set(model.tableId));
      }
      if (model.userId != null) {
        ret.add(userId.set(model.userId));
      }
      if (model.status != null) {
        ret.add(status.set(model.status));
      }
      if (model.dateCreated != null) {
        ret.add(dateCreated.set(model.dateCreated));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists: false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(id.name, primary: true, autoIncrement: true, isNullable: false);
    st.addStr(remarks.name, isNullable: true);
    st.addInt(tableId.name, isNullable: true);
    st.addInt(userId.name, isNullable: true);
    st.addInt(status.name, isNullable: false);
    st.addDateTime(dateCreated.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Order model,
      {bool cascade: false, bool onlyNonNull: false, Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(id.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      Order newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<Order> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Order model,
      {bool cascade: false, Set<String> only, bool onlyNonNull: false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(id.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      Order newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<Order> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Order model,
      {bool cascade: false,
      bool associate: false,
      Set<String> only,
      bool onlyNonNull: false}) async {
    final Update update = updater
        .where(this.id.eq(model.id))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Order> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.id.eq(model.id));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Order> find(int id, {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.id.eq(id));
    return await findOne(find);
  }

  Future<int> remove(int id) async {
    final Remove remove = remover.where(this.id.eq(id));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Order> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.id.eq(model.id));
    }
    return adapter.remove(remove);
  }
}

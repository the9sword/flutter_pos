// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Payment.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _PaymentBean implements Bean<Payment> {
  final id = IntField('id');
  final orderId = IntField('order_id');
  final paymentType = IntField('payment_type');
  final remarks = StrField('remarks');
  final status = IntField('status');
  final loyaltyUser = StrField('loyalty_user');
  final dateCreated = DateTimeField('date_created');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        id.name: id,
        orderId.name: orderId,
        paymentType.name: paymentType,
        remarks.name: remarks,
        status.name: status,
        loyaltyUser.name: loyaltyUser,
        dateCreated.name: dateCreated,
      };
  Payment fromMap(Map map) {
    Payment model = Payment();
    model.id = adapter.parseValue(map['id']);
    model.orderId = adapter.parseValue(map['order_id']);
    model.paymentType = adapter.parseValue(map['payment_type']);
    model.remarks = adapter.parseValue(map['remarks']);
    model.status = adapter.parseValue(map['status']);
    model.loyaltyUser = adapter.parseValue(map['loyalty_user']);
    model.dateCreated = adapter.parseValue(map['date_created']);

    return model;
  }

  List<SetColumn> toSetColumns(Payment model,
      {bool update = false, Set<String> only, bool onlyNonNull: false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.id != null) {
        ret.add(id.set(model.id));
      }
      ret.add(orderId.set(model.orderId));
      ret.add(paymentType.set(model.paymentType));
      ret.add(remarks.set(model.remarks));
      ret.add(status.set(model.status));
      ret.add(loyaltyUser.set(model.loyaltyUser));
      ret.add(dateCreated.set(model.dateCreated));
    } else if (only != null) {
      if (model.id != null) {
        if (only.contains(id.name)) ret.add(id.set(model.id));
      }
      if (only.contains(orderId.name)) ret.add(orderId.set(model.orderId));
      if (only.contains(paymentType.name))
        ret.add(paymentType.set(model.paymentType));
      if (only.contains(remarks.name)) ret.add(remarks.set(model.remarks));
      if (only.contains(status.name)) ret.add(status.set(model.status));
      if (only.contains(loyaltyUser.name))
        ret.add(loyaltyUser.set(model.loyaltyUser));
      if (only.contains(dateCreated.name))
        ret.add(dateCreated.set(model.dateCreated));
    } else /* if (onlyNonNull) */ {
      if (model.id != null) {
        ret.add(id.set(model.id));
      }
      if (model.orderId != null) {
        ret.add(orderId.set(model.orderId));
      }
      if (model.paymentType != null) {
        ret.add(paymentType.set(model.paymentType));
      }
      if (model.remarks != null) {
        ret.add(remarks.set(model.remarks));
      }
      if (model.status != null) {
        ret.add(status.set(model.status));
      }
      if (model.loyaltyUser != null) {
        ret.add(loyaltyUser.set(model.loyaltyUser));
      }
      if (model.dateCreated != null) {
        ret.add(dateCreated.set(model.dateCreated));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists: false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(id.name, primary: true, autoIncrement: true, isNullable: false);
    st.addInt(orderId.name, isNullable: false);
    st.addInt(paymentType.name, isNullable: false);
    st.addStr(remarks.name, isNullable: true);
    st.addInt(status.name, isNullable: false);
    st.addStr(loyaltyUser.name, isNullable: true);
    st.addDateTime(dateCreated.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Payment model,
      {bool cascade: false, bool onlyNonNull: false, Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(id.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      Payment newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<Payment> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Payment model,
      {bool cascade: false, Set<String> only, bool onlyNonNull: false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(id.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      Payment newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<Payment> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Payment model,
      {bool cascade: false,
      bool associate: false,
      Set<String> only,
      bool onlyNonNull: false}) async {
    final Update update = updater
        .where(this.id.eq(model.id))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Payment> models,
      {bool onlyNonNull: false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.id.eq(model.id));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Payment> find(int id,
      {bool preload: false, bool cascade: false}) async {
    final Find find = finder.where(this.id.eq(id));
    return await findOne(find);
  }

  Future<int> remove(int id) async {
    final Remove remove = remover.where(this.id.eq(id));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Payment> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.id.eq(model.id));
    }
    return adapter.remove(remove);
  }
}

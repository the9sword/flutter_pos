import 'dart:async';
import 'package:jaguar_query/jaguar_query.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
// import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

part 'Order.jorm.dart';

enum OrderStatus {
  Order,
  Hold,
  OrderConfirm,
  Cooked,
  Paid,
  TakeAway,
  Deleted,
}

String getOrderStatusTitle(int order) {
  switch (order) {
    case 0:
      {
        return "Order";
      }
    case 1:
      {
        return "Hold";
      }
    case 2:
      {
        return "Ordered";
      }
    case 3:
      {
        return "Cooked";
      }
    case 4:
      {
        return "Paid";
      }
    case 5:
      {
        return "TakeAway";
      }
    case 6:
      {
        return "Deleted";
      }
  }
}

// The model
class Order {
  Order();

  Order.make(this.id, this.remarks,this.tableId,this.userId, this.status, this.dateCreated);

  @PrimaryKey(auto: true)
  int id;

  @Column(isNullable: true)
  String remarks;

  @Column(isNullable: true)
  int tableId;

  @Column(isNullable: true)
  int userId;

  @Column(isNullable: false)
  int status;

  @Column(isNullable: false)
  DateTime dateCreated;




}

@GenBean()
class OrderBean extends Bean<Order> with _OrderBean {
  OrderBean(Adapter adapter) : super(adapter);

  Future<int> updateReadField(int id, bool read) async {
    //Update st = updater.where(this.id.eq(id)).set(this.read, read);
    //return adapter.update(st);
  }

  final String tableName = 'pos_order';
}
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pos/firstFragment.dart';
import 'package:pos/Home.dart';
import 'package:pos/Dao/ResTable.dart';
import 'DatabaseCall.dart';
class TableFragment extends StatefulWidget {

  final StreamController tableStreamController;
  //final OrderObservable item;
  final homeObservable observable;
  TableFragment(this.tableStreamController, this.observable);

  @override
  _TableFragmentState createState() => _TableFragmentState();
}

class _TableFragmentState extends State<TableFragment> {

  List<ResTable> list;

  final GridViewRow = 6;

  @override
  initState() {
    super.initState();
    onLoadData();
  }

  _onTapImage(BuildContext context, String url) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        new Image.asset(
          "assets/" + url,
          fit: BoxFit.fill,
          height: 100,
        ), // Show your Image
        Align(
          alignment: Alignment.topRight,
          child: RaisedButton.icon(
              color: Theme
                  .of(context)
                  .accentColor,
              textColor: Colors.white,
              onPressed: () => Navigator.pop(context),
              icon: Icon(
                Icons.close,
                color: Colors.white,
              ),
              label: Text('Close')),
        ),
      ],
    );
  }

  _showDialog(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      //this right here
      child: Container(
        height: 300.0,
        width: 300.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(15.0),
              child: Text(
                'Cool',
                style: TextStyle(color: Colors.red),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(15.0),
              child: Text(
                'Awesome',
                style: TextStyle(color: Colors.red),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 50.0)),
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Got It!',
                  style: TextStyle(color: Colors.purple, fontSize: 18.0),
                ))
          ],
        ),
      ),
    );
  }

  void onLoadData() async {
    var listTemp = await getAllTable();

    setState(() {
      list = listTemp;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: new GridView.builder(
            itemCount: list == null ? 0 : list.length,
            gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: GridViewRow),
            itemBuilder: (BuildContext context, int index) {
              var tableNo = list[index].number;
              return getBuildUi(context, index, tableNo);
            }),
      ),
    );
  }

  getBuildUi(context, index ,tableNo) {
    return FutureBuilder(
        future: getTableOrderStatus(list[index].id),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return Padding(
              padding: EdgeInsets.all(8.0),
              child: new RawMaterialButton(
                onPressed: () {
                  widget.tableStreamController.add("0");

                 // widget.item.add1("lollll");
                  widget.observable.add(list[index].id);
                },
                child: new Text(
                    "Table $tableNo"
                ),
                shape: new CircleBorder(),
                elevation: 2.0,
                fillColor:  snapshot.data['isOrdered']==true ? Colors.red : Colors.white,
                padding: const EdgeInsets.all(15.0),
              ),
            );
          } else {
            return Padding(
                padding: EdgeInsets.all(16.0),
                child: Center(
                  child: CircularProgressIndicator(),
                ));
          }
        });
  }

}
class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'hmmm', icon: Icons.directions_car),
  const Choice(title: 'buy buy buy!', icon: Icons.directions_bike),
  const Choice(title: 'yooooo', icon: Icons.directions_boat),
];

class ChoiceCard extends StatelessWidget {
  const ChoiceCard({Key key, this.choice}) : super(key: key);

  final Choice choice;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.display1;
    return Card(
      color: Colors.white,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(choice.icon, size: 128.0, color: textStyle.color),
            Text(choice.title, style: textStyle),
          ],
        ),
      ),
    );
  }
}

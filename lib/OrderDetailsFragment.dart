import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:pos/CustomExpansionTile.dart';
import 'package:pos/Dao/Order.dart';
import 'package:pos/Dao/OrderItem.dart';
import 'package:pos/DatabaseCall.dart';
import 'package:pos/Home.dart';
import 'package:pos/MyState.dart';

class OrderDetailFragment extends StatefulWidget {
  final StreamController<MyState> streamController;
  final StreamController<int> onOrderChangeController;
  final MyHomePageState myState;
  var state;
  bool isShow;
  bool isEditable;
  var tableNo;

  OrderDetailFragment(
      this.streamController,  this.onOrderChangeController,this.myState,
      {Key key})
      : super(key: key);

  @override
  OrderDetailFragmentState createState() {
    streamController.stream.listen((data) {
      tableNo = data.TableId;
      state.onLoadData(tableNo);
    }, onDone: () {
      print("Task Done1");
    }, onError: (error) {
      print("Some Error1");
    });
    onOrderChangeController.stream.listen((orderStatus) {
      switch (orderStatus) {
        case 1:
          {
            state. updateOrder(OrderStatus.OrderConfirm);
          }
          break;
        case 2:
          {
            state. updateOrder(OrderStatus.Hold);
          }
          break;
        case 3:
          {
            state.cancelOrder();
          }
          break;
        default:
          {
            //statements;
          }
          break;
      }
    }, onDone: () {
      print("Task Done1");
    }, onError: (error) {
      print("Some Error1");
    });
    return state = new OrderDetailFragmentState();
  }
}

class OrderDetailFragmentState extends State<OrderDetailFragment> {
  List<OrderItem> list;
  ScrollController _controller = ScrollController();
  bool showOrder = true;
  bool show = true;
  int countOnTakeAway = 0;
  double total = 0.0;
  double subtotal = 0.0;
  double totaltax = 0.0;
  double discount = 0.0;
  final String textTaxTakeAway = "1.4%";
  final String textTaxDineIn = "1.6%";
  final double tax_rate_take_away = 0.014;
  final double tax_rate_dine_in = 0.016;
  final double listItemHeight = 40;

  void showSnack() {
    setState(() {});
  }

  void onLoadData(int tableNo) async {
    if (tableNo == null) {
      return;
    }

    List<OrderItem> listTemp = await getOrderItemListByTableNo(tableNo);
    var tempsubtotal = 0.0;
    var temptotaltax = 0.0;
    var temptotal = 0.0;
    if (listTemp != null)
      for (var i = 0; i < listTemp.length; i++) {
        var price = await getProductPriceByProductId(listTemp[i].productId);
        tempsubtotal += price;
        print("price $i - " + price.toString());
        if (listTemp[i].status != OrderStatus.TakeAway.index) {
          temptotaltax += price * tax_rate_take_away;
        } else {
          temptotaltax += price * tax_rate_dine_in;
        }
      }
    temptotal = tempsubtotal + temptotaltax;
    setState(() {
      list = listTemp;
      subtotal = tempsubtotal;
      totaltax = temptotaltax;
      total = temptotal;
    });
  }

  // var _radioValue;
  int _selectedPayment;

  void _handleRadioValueChange(int value) {
    setState(() {
      //_radioValue = value;
      // _selectedPayment = value;
    });
  }

  _showDialog(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      //this right here
      child: Container(
        height: 600.0,
        width: 600.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Radio(
                  value: 0,
                  groupValue: _selectedPayment,
                  onChanged: _handleRadioValueChange,
                ),
                Padding(
                    padding: EdgeInsets.all(15.0),
                    child: new Container(
                        height: 120,
                        width: 120,
                        child: new Image.asset(
                          "assets/frame.png",
                          fit: BoxFit.fitHeight,
                        ))),
              ],
            ),
            new Row(
              children: <Widget>[
                new Radio(
                  value: 1,
                  groupValue: _selectedPayment,
                  onChanged: _handleRadioValueChange,
                ),
                Padding(
                    padding: EdgeInsets.all(15.0),
                    child: new Container(
                        height: 120,
                        width: 320,
                        child: new Image.asset(
                          "assets/credit.jpg",
                          fit: BoxFit.fitWidth,
                        ))),
              ],
            ),
            new Row(
              children: <Widget>[
                new Radio(
                  value: 2,
                  groupValue: _selectedPayment,
                  onChanged: _handleRadioValueChange,
                ),
                Padding(
                    padding: EdgeInsets.all(15.0),
                    child: new Container(
                        height: 120,
                        width: 120,
                        child: new Image.asset(
                          "assets/cash.jpg",
                          fit: BoxFit.fitWidth,
                        ))),
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 50.0)),
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Got It!',
                  style: TextStyle(color: Colors.purple, fontSize: 18.0),
                ))
          ],
        ),
      ),
    );
  }

  cancelOrder() async {
    Order order = await getActiveOrderByTableNo(widget.tableNo);
    await deleteOrder(order);
    setState(() {
      list.clear();
      total = 0.0;
      subtotal = 0.0;
      totaltax = 0.0;
      discount = 0.0;
    });

    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          //  title: new Text("Alert Dialog title"),
          content: new Text("Order canceled."),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  updateOrder(OrderStatus orderStatus) async {
    await updateOrderItemToConfirm(list, orderStatus);

    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          //  title: new Text("Alert Dialog title"),
          content: new Text(orderStatus == OrderStatus.OrderConfirm
              ? "Your Order has been confirmed."
              : "Your Order has been hold."),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void calculateTotal(int tableNo) async {
    List<OrderItem> listTemp = await getOrderItemListByTableNo(tableNo);
    var tempsubtotal = 0.0;

    var temptotaltax = 0.0;
    var temptotal = 0.0;
    for (var i = 0; i < listTemp.length; i++) {
      var price = await getProductPriceByProductId(listTemp[i].productId);
      tempsubtotal += price;
      print("price $i - " + price.toString());
      if (listTemp[i].status != OrderStatus.TakeAway.index) {
        temptotaltax += price * tax_rate_take_away;
      } else {
        temptotaltax += price * tax_rate_dine_in;
      }
    }
    temptotal = tempsubtotal + temptotaltax;
    setState(() {
      list = listTemp;
      subtotal = tempsubtotal;
      totaltax = temptotaltax;
      total = temptotal;
    });
  }

  @override
  void initState() {
    super.initState();
    _controller.addListener(listener);
    _collapse();
  }

  @override
  Widget build(BuildContext context) {
    int tableNum = widget.tableNo;

    return new StreamBuilder<MyState>(
        stream: widget.streamController.stream,
        builder: (BuildContext context, AsyncSnapshot<MyState> snapshot) {
          //final playerState = snapshot.data.TableId.toString();
          if (snapshot.hasData)
            return new Stack(children: <Widget>[
              new Padding(
                padding:
                    EdgeInsets.only(left: 0, top: 16, right: 16, bottom: 16),
                child: new Material(
                  elevation: 6,
                  child: new Column(
                    children: <Widget>[
                      new Padding(
                        padding: EdgeInsets.all(8.0),
                        child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new Text(
                                  widget.tableNo == 0
                                      ? "Take Away"
                                      : "Table No : $tableNum",
                                  style: new TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.normal)),
                              new Text("status",
                                  style: new TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.normal))
                            ]),
                      ),
//              new Padding(
//                  padding: EdgeInsets.only(left: 12.0, right: 12.0),
//                  child: Container(height: 0.5, color: Colors.grey)),
                      new Container(
                        color: Colors.black12,
                        child: new Padding(
                          padding: EdgeInsets.all(8.0),
                          child: new Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                new Text("Quantity ",
                                    style: new TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.normal)),
                                new Padding(
                                    padding:
                                        EdgeInsets.only(left: 18, right: 8),
                                    child: new Text("Price",
                                        style: new TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.normal)))
                              ]),
                        ),
                      ),
//              new Padding(
//                  padding: EdgeInsets.only(left: 12.0, right: 12.0),
//                  child: Container(height: 0.5, color: Colors.grey)),
                      new Expanded(
                        child: ListView.separated(
                          separatorBuilder: (context, index) => new Padding(
                                padding: EdgeInsets.only(left: 8.0, right: 8.0),
                                child:
                                    Container(height: 0.5, color: Colors.grey),
                              ),
                          controller: _controller,
                          itemCount: list == null ? 0 : list.length,
                          itemBuilder: (context, index) {
                            return _getItemUI(context, index);
                          },
                        ),
                      ),
                      new Align(
                          alignment: FractionalOffset.bottomRight,
                          child: new Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                new Container(
                                    color: Colors.black12,
                                    height: 48,
                                    child: new Padding(
                                        padding: EdgeInsets.only(top: 16),
                                        child: new Stack(children: <Widget>[
                                          new Positioned(
                                            left: 16.0,
                                            child: new Text("Discount ",
                                                style: new TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: 16.0,
                                                    fontWeight:
                                                        FontWeight.normal)),
                                          ),
                                          new Positioned(
                                            right: 16.0,
                                            child: new Container(
                                                height: 26,
                                                width: 40,
                                                child: TextField(
                                                  inputFormatters: <
                                                      TextInputFormatter>[
                                                    WhitelistingTextInputFormatter
                                                        .digitsOnly
                                                  ],
                                                  keyboardType:
                                                      TextInputType.number,
                                                  style: new TextStyle(
                                                      fontSize: 14.0,
                                                      height: 1.0,
                                                      color: Colors.black),
                                                  decoration: InputDecoration(
                                                      contentPadding:
                                                          const EdgeInsets
                                                                  .symmetric(
                                                              vertical: 4,
                                                              horizontal: 6.0),
                                                      border:
                                                          OutlineInputBorder()),
                                                  onChanged: (text) {
                                                    print("ssss");
                                                    var tempdiscount = text
                                                            .isEmpty
                                                        ? 0
                                                        : double.parse(text);
                                                    var temptotal = (subtotal -
                                                            tempdiscount) +
                                                        totaltax;

                                                    setState(() {
                                                      total = temptotal;
                                                    });
                                                  },
                                                )),
                                          ),
                                        ]))),
                                new Container(
                                    color: Colors.black12,
                                    height: 40,
                                    child: new Padding(
                                        padding: EdgeInsets.only(top: 4),
                                        child: new Stack(children: <Widget>[
                                          new Positioned(
                                            left: 16.0,
                                            child: new Text("Sub Total ",
                                                style: new TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: 16.0,
                                                    fontWeight:
                                                        FontWeight.normal)),
                                          ),
                                          new Positioned(
                                            right: 16.0,
                                            child: new Text(
                                                subtotal.toStringAsFixed(2),
                                                style: new TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: 16.0,
                                                    fontWeight:
                                                        FontWeight.normal)),
                                          ),
                                        ]))),
                                new Container(
                                    color: Colors.black12,
                                    height: 36,
                                    child: new Padding(
                                        padding:
                                            EdgeInsets.only(top: 0, bottom: 0),
                                        child: new Stack(children: <Widget>[
                                          new Positioned(
                                            left: 16.0,
                                            child: new Text(
                                                "Tax :" +
                                                    (widget.tableNo == 0
                                                        ? textTaxTakeAway
                                                        : textTaxDineIn),
                                                style: new TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: 16.0,
                                                    fontWeight:
                                                        FontWeight.normal)),
                                          ),
                                          new Positioned(
                                            right: 16.0,
                                            child: new Text(
                                                totaltax.toStringAsFixed(2),
                                                style: new TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: 16.0,
                                                    fontWeight:
                                                        FontWeight.normal)),
                                          ),
                                        ]))),
                                new Container(
                                    color: Colors.white,
                                    height: 30,
                                    child: new Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: new Stack(children: <Widget>[
                                          new Positioned(
                                            left: 16.0,
                                            child: new Text("Total :",
                                                style: new TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: 22.0,
                                                    fontWeight:
                                                        FontWeight.normal)),
                                          ),
                                          new Positioned(
                                            right: 16.0,
                                            child: new Text(
                                                total.toStringAsFixed(2),
                                                style: new TextStyle(
                                                    color: Colors.green,
                                                    fontSize: 22.0,
                                                    fontWeight:
                                                        FontWeight.normal)),
                                          ),
                                        ]))),
                                new Container(
                                  color: Colors.white,
                                  height: 58,
                                  child: new Padding(
                                    padding: EdgeInsets.all(8),
                                    child: new RaisedButton(
                                      onPressed: () async {
                                        if (await checkCartIsEmptyProcess(
                                                widget.tableNo ?? -1) !=
                                            true) {
                                          showDialog(
                                              context: context,
                                              builder: (context) =>
                                                  _showDialog(context));
                                        }
                                      },
                                      color: Colors.green,
                                      padding: EdgeInsets.all(4.0),
                                      child: Center(
                                        // Replace with a Row for horizontal icon + text
                                        child: Text(
                                            "PAY ${total.toStringAsFixed(2)}",
                                            style: new TextStyle(
                                                color: Colors.white)),
                                      ),
                                    ),
                                  ),
                                ),
                              ]))
                    ],
                  ),
                ),
              ),
//              new Align(
//                alignment: FractionalOffset.bottomCenter,
//                child: new Padding(
//                  padding: EdgeInsets.only(right: 16, bottom: 16),
//                  child: new Material(
//                    elevation: 6,
//                    child: new Container(
//                      height: 55,
//                      child: new Padding(
//                        padding: EdgeInsets.all(4),
//                        child: new Row(
//                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                          children: <Widget>[
//                            new RaisedButton(
//                              onPressed: cancelOrder,
//                              color: Colors.redAccent,
//                              padding: EdgeInsets.all(4.0),
//                              child: Center(
//                                // Replace with a Row for horizontal icon + text
//                                child: Text("CANCEL\nORDER",
//                                    style: new TextStyle(color: Colors.white)),
//                              ),
//                            ),
//                            new OutlineButton(
//                              borderSide: BorderSide(color: Colors.lightGreen),
//                              shape: new RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(2.0)),
//                              onPressed: () {
//                                updateOrder(OrderStatus.Hold);
//                              },
//                              color: Colors.white,
//                              padding: EdgeInsets.all(4.0),
//                              child: Center(
//                                // Replace with a Row for horizontal icon + text
//                                child: Text("HOLD\nORDER",
//                                    style: new TextStyle(
//                                        color: Colors.lightGreen)),
//                              ),
//                            ),
//                            new OutlineButton(
//                              onPressed: () {
//                                updateOrder(OrderStatus.OrderConfirm);
//                              },
//                              //color: Colors.greenAccent,
//                              borderSide: BorderSide(color: Colors.green),
//                              shape: new RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(2.0)),
//                              padding: EdgeInsets.all(4.0),
//                              child: Center(
//                                // Replace with a Row for horizontal icon + text
//                                child: Text("PLACE\nORDER",
//                                    style: new TextStyle(color: Colors.green)),
//                              ),
//                            ),
//                          ],
//                        ),
//                      ),
//                    ),
//                  ),
//                ),
//              )
            ]);
          else
            return new Container();
        });
  }

  @override
  void dispose() {
    _controller.removeListener(listener);
    super.dispose();
  }

  void listener() {
    if (_controller.position.userScrollDirection == ScrollDirection.forward)
      show = true;
    else
      show = false;

    setState(() {});
  }

  String foos = 'One';
  int _key;

  _collapse() {
    int newKey;
    do {
      _key = new Random().nextInt(10000);
    } while (newKey == _key);
  }

  _getItemUI(BuildContext context, int index) {
    return FutureBuilder(
      future: getProductByProductId(list[index].productId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          if (list[index].status != OrderStatus.OrderConfirm.index)
            return Dismissible(
                // Each Dismissible must contain a Key. Keys allow Flutter to
                // uniquely identify Widgets.
                key: Key(list[index].id.toString()),
                // We also need to provide a function that tells our app
                // what to do after an item has been swiped away.
                onDismissed: (direction) {
                  deleteOrderItem(list[index]);
                  calculateTotal(widget.tableNo);
                  setState(() {
                    list.removeAt(index);
                  });
                },
                // Show a red background as the item is swiped away
                background: Container(
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(left: 20.0),
                  color: Colors.redAccent,
                  child: Icon(Icons.delete, color: Colors.white),
                ),
                child: itemLayout(context, index, snapshot));
          else {
            return itemLayout(context, index, snapshot);
          }
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  itemLayout(BuildContext context, int index, snapshot) {
    return new CustomExpansionTile(
        key: new Key(_key.toString()),
        initiallyExpanded: false,
        title: Container(
          child: new Stack(children: <Widget>[
            Container(
              height: listItemHeight,
              child: new Align(
                alignment: Alignment.centerLeft,
                child: new Row(children: <Widget>[
                  new Padding(
                    padding: EdgeInsets.only(right: 16),
                    child: new Text("${index + 1}",
                        style: new TextStyle(
                            color: Colors.grey,
                            fontSize: 12.0,
                            fontWeight: FontWeight.normal)),
                  ),
                  new Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Text(snapshot.data['name'] ?? '',
                            style: new TextStyle(
                                fontSize: 16.0, fontWeight: FontWeight.normal)),

                        new Container(
                            child: new Text(
                                getOrderStatusTitle(list[index].status),
                                style: new TextStyle(
                                    color: Colors.white,
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.normal)),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 4.0, vertical: 2.0),
                            decoration: const BoxDecoration(
                              border: Border(
                                top: BorderSide(
                                    width: 1.0, color: Color(0xFFFFDFDFDF)),
                                left: BorderSide(
                                    width: 1.0, color: Color(0xFFFFDFDFDF)),
                                right: BorderSide(
                                    width: 1.0, color: Color(0xFFFF7F7F7F)),
                                bottom: BorderSide(
                                    width: 1.0, color: Color(0xFFFF7F7F7F)),
                              ),
                              color: Colors.red,
                            )),

//                              list[index].remarks != null
//                                  ? new Text(list[index].remarks.toString(),
//                                      style: new TextStyle(
//                                          fontSize: 10.0,
//                                          fontWeight: FontWeight.normal))
//                                  : null,
                      ]),
                ]),
              ),
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  height: listItemHeight,
                  width: 106.0,
                  child: new Center(
                      child: new Row(
                    children: <Widget>[
                      new IconButton(
                          icon:
                              new Icon(Icons.remove_circle_outline, size: 18.0),
                          color: Colors.lightGreen,
                          onPressed: () => {}),
                      new Text(list[index].quantity.toString() ?? '',
                          style: new TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.normal)),
                      new IconButton(
                          icon: new Icon(Icons.add_circle_outline, size: 18.0),
                          color: Colors.lightGreen,
                          onPressed: () => {}),
                    ],
                  )),
                ),
                Container(
                  width: 55.0,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: new Text(snapshot.data['price'] ?? '',
                        style: new TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.normal)),
                  ),
                ),
              ],
            ),
          ]),
        ),
        backgroundColor: Theme.of(context).accentColor.withOpacity(0.025),
        children: [
          new Container(
              color: Colors.black12,
              height: 50,
              child: new Padding(
                  padding: EdgeInsets.only(top: 22),
                  child: new Stack(children: <Widget>[
                    new Positioned(
                      left: 16.0,
                      child: new Text("Discount ",
                          style: new TextStyle(
                              color: Colors.black87,
                              fontSize: 16.0,
                              fontWeight: FontWeight.normal)),
                    ),
                    new Positioned(
                      right: 16.0,
                      child: new Container(
                          height: 26,
                          width: 40,
                          child: TextField(
                            inputFormatters: <TextInputFormatter>[
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            keyboardType: TextInputType.number,
                            style: new TextStyle(
                                fontSize: 14.0,
                                height: 1.0,
                                color: Colors.black),
                            decoration: InputDecoration(
                                contentPadding: const EdgeInsets.symmetric(
                                    vertical: 4, horizontal: 6.0),
                                border: OutlineInputBorder()),
                            onChanged: (text) {
                              print("ssss");
                              var tempdiscount =
                                  text.isEmpty ? 0 : double.parse(text);
                              var temptotal =
                                  (subtotal - tempdiscount) + totaltax;

                              setState(() {
                                total = temptotal;
                              });
                            },
                          )),
                    ),
                  ]))),
          new Container(
              color: Colors.black12,
              height: 50,
              child: new Padding(
                  padding: EdgeInsets.only(bottom: 22),
                  child: new Stack(children: <Widget>[
                    new Positioned(
                      left: 16.0,
                      child: new Text("Remark ",
                          style: new TextStyle(
                              color: Colors.black87,
                              fontSize: 16.0,
                              fontWeight: FontWeight.normal)),
                    ),
                    new Positioned(
                      right: 16.0,
                      child: new Container(
                          height: 26,
                          width: 200,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            style: new TextStyle(
                                fontSize: 14.0,
                                height: 1.0,
                                color: Colors.black),
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(
                                  vertical: 4, horizontal: 6.0),
                            ),
                            onChanged: (text) {
                              var tempdiscount =
                                  text.isEmpty ? 0 : double.parse(text);
                              var temptotal =
                                  (subtotal - tempdiscount) + totaltax;

                              setState(() {
                                total = temptotal;
                              });
                            },
                          )),
                    ),
                  ]))),
        ]);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    // TODO: implement updateShouldNotify
    return null;
  }
}

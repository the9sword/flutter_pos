class MyState {
  final int TableId;
  final int GridCount;

  MyState.initial({this.TableId = -1, this.GridCount = 4});

  MyState.changeText(this.TableId, this.GridCount);

}

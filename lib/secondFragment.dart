import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pos/CustomTab.dart';
import 'package:pos/DatabaseCall.dart';
import 'package:pos/Home.dart';

class SecondFragment extends StatefulWidget {

  final homeObservable observable;
  StreamController<String> streamController1;
  void showSnack() {
    state.showSnack();
  }

  SecondFragment(this.observable, this.streamController1, {Key key}) : super(key: key);
  SecondFragmentState state ;
  @override
  SecondFragmentState createState(){

  return state = SecondFragmentState() ;
  }
}

class SecondFragmentState extends State<SecondFragment> {

  SecondFragmentState myState;
  GlobalKey<SecondFragmentState> myWidgetStateKey ;

  BuildContext context1;
  List<BillType> listTitle = [
    BillType.Order,
    BillType.Payment,
    BillType.History
  ];


  List<String> list = ["img1.jpg", "img2.jpg", "img3.jpg"];
  // Widget imgView ;
  String text = "llllll";
  bool _visible = true;
  Stream<bool> stream;
  void showSnack() {
   getData();
  }

  void onTabTapped(int index) {
    var str = listTitle[index].toString();
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text('Tap $str'),
    ));
    onLoadData(listTitle[index]);
  }
  void onLoadData(BillType obj) async {
    var listTemp = await getAllBillByCategory(obj);
    // print("haha");
    // print(listTemp.length);

  }
  @override
  SecondFragmentState createState(){
    myState = new SecondFragmentState();
    return myState;
  }

  static SecondFragmentState of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(SecondFragmentState)
    as SecondFragmentState)
        .myState;
  }

  @override
  void initState() {
    super.initState();
    myWidgetStateKey = new GlobalKey<SecondFragmentState>();
    //First subscription
    widget.streamController1.stream.listen((data) {
      getData();
      print("DataReceived1jjj: " + data);
    }, onDone: () {
      print("Task Done1");
    }, onError: (error) {
      print("Some Error1");
    });


//    print("Creating a sample stream...");
   //  stream = new Stream.fromFuture(getData());
//    print("Created the stream");
//
//    stream.listen((data) {
//      print("DataReceived: "+data);
//    }, onDone: () {
//      print("Task Done");
//    }, onError: (error) {
//      print("Some Error");
//    });
//
//    print("code controller is here");
  }

  Future getData() async  {
   // await Future.delayed(Duration(milliseconds: 300)); //Mock delay
    print("Fetched Datanjjjjjj");
    //if (mounted) {
    myWidgetStateKey.currentState.setState(() {
      _visible = _visible ? false : true;
    });
    myState.setState(() {
        _visible = _visible ? false : true;
        text = "assssss";
      });
    //}
    //return false;
  }
  @override
  Widget build(BuildContext context) {
    this.context1 = context;
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        key: myWidgetStateKey,
        body: new Column(children: <Widget>[
          new  AnimatedOpacity(
        // If the Widget should be visible, animate to 1.0 (fully visible). If
        // the Widget should be hidden, animate to 0.0 (invisible).
        opacity: _visible ? 1.0 : 0.0,
            duration: Duration(milliseconds: 500),
            // The green box needs to be the child of the AnimatedOpacity
            child:
          new Container(
            height: 50,
            decoration: new BoxDecoration(color: Colors.transparent),
            child: CustomTabBar(
              onTap: onTabTapped,
              labelColor: Colors.white,
              unselectedLabelColor: Colors.blue,
              indicator: CustomTabIndicator(),
              tabs: [
                Tab(text: getBillCategoryTitle(listTitle[0])),
                Tab(
                  text: text,
                ),
                Tab(
                  text: getBillCategoryTitle(listTitle[2]),
                ),
              ],
            ),
          ),
          ),

          new Expanded(
            child: ListView.builder(
              itemCount: list == null ? 0 : list.length,
              itemBuilder: _getItemUI,
              padding: EdgeInsets.all(0.0),
            ),
          ),
        ]),
      ),
    );
  }

  Widget _getItemUI(BuildContext context, int index) {
    text = list[index];
    return new Card(
      clipBehavior: Clip.antiAlias,
      child: new InkWell(
        // When the user taps the button, show a snackbar
        onTap: () {
          setState(() {
            //  imgView = new Container();
            list[index] = 'hahah';
            list.add("hmmmm");
          });

          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Tap'),
          ));
        },
        child: new Row(
          children: <Widget>[
//            ClipRRect(
//              borderRadius: new BorderRadius.circular(4.0),
//              child:
            new Image.asset(
              "assets/" + list[index],
              fit: BoxFit.cover,
              width: 80.0,
              height: 80,
            ),
            // ),
            new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(text,
                      style: new TextStyle(
                          fontSize: 13.0, fontWeight: FontWeight.normal)),
                  new Text('Population: ${list[index]}',
                      style: new TextStyle(
                          fontSize: 11.0, fontWeight: FontWeight.normal)),
                ]),
          ],
        ),
      ),
    );
  }
}

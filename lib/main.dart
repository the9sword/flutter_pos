import 'dart:async';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:path/path.dart' as path;
import 'package:pos/Dao/Order.dart';
import 'package:pos/Dao/OrderItem.dart';
import 'package:pos/Dao/Product.dart';
import 'package:pos/Dao/ResTable.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:sqflite/sqflite.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'Dao/PosUser.dart';
import 'DatabaseCall.dart';
import 'Home.dart';

void main() {
  //screen lock
  SystemChrome.setPreferredOrientations([
    //DeviceOrientation.portraitUp,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight,
  ]).then((_) {
    runApp(new MyLogin());
  });
}

SqfliteAdapter _adapter;

class MyLogin extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.green,
      ),
      // home: WebViewExample(),
      home: MyHomePage(title: 'POS'),
    );
  }
}

void setupDatabase() async {
  try {
    var dbPath = await getDatabasesPath();
    _adapter = new SqfliteAdapter(path.join(dbPath, "my_pos_flutter.db"));
    await _adapter.connect();

    final bean = ProductBean(_adapter);

    try {
      var list = await bean.getAll();
      if (list.length > 0) {
        return;
      }
    } catch (e) {
      print(e);
    }
    final orderBean = OrderBean(_adapter);
    final orderItemBean = OrderItemBean(_adapter);
    final tableBean = ResTableBean(_adapter);
    final userBean = PosUserBean(_adapter);

    await bean.drop();
    await orderBean.drop();
    await orderItemBean.drop();
    await tableBean.drop();
    await userBean.drop();

    await bean.createTable();
    await orderBean.createTable();
    await orderItemBean.createTable();
    await tableBean.createTable();
    await userBean.createTable();

    await bean.removeAll();
    await orderBean.removeAll();
    await orderItemBean.removeAll();
    await tableBean.removeAll();
    await userBean.removeAll();

    await userBean.insert(new PosUser.make(1122111, UserRole.Manager.index,
        "Yen", "admin", "abc123", "new Branch", "nothing", DateTime.now()));

    await tableBean.insert(new ResTable.make(null, "1", true, 100, 500));
    await tableBean.insert(new ResTable.make(null, "2", true, 180, 500));
    await tableBean.insert(new ResTable.make(null, "3", true, 280, 500));
    await tableBean.insert(new ResTable.make(null, "4", true, 370, 500));
    await tableBean.insert(new ResTable.make(null, "5", true, 440, 500));

    await tableBean.insert(new ResTable.make(null, "6", true, 470, 420));
    await tableBean.insert(new ResTable.make(null, "7", true, 410, 420));
    await tableBean.insert(new ResTable.make(null, "8", true, 340, 420));
    await tableBean.insert(new ResTable.make(null, "9", true, 240, 420));
    await tableBean.insert(new ResTable.make(null, "10", true, 170, 420));
    await tableBean.insert(new ResTable.make(null, "11", true, 167, 335));
    await tableBean.insert(new ResTable.make(null, "12", true, 234, 335));
    await tableBean.insert(new ResTable.make(null, "13", true, 310, 335));
    await tableBean.insert(new ResTable.make(null, "14", true, 380, 335));
    await tableBean.insert(new ResTable.make(null, "15", true, 450, 335));

    await bean.insert(new Product.make(1, 'Nasi Lemak', Category.Rice.index,
        "mamak king", "img1.jpg", 11.20, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(7, 'Nasi Lemak 2', Category.Rice.index,
        "mamak king", "img1.jpg", 15.85, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(8, 'Nasi Lemak 3', Category.Rice.index,
        "mamak king", "img1.jpg", 12.99, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(9, 'Nasi Lemak 4', Category.Rice.index,
        "mamak king", "img1.jpg", 10.90, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(10, 'Nasi Lemak 5', Category.Rice.index,
        "mamak king", "img1.jpg", 12.80, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(2, 'Sky Juice', Category.Breverage.index,
        "mamak king2", "img2.jpg", 13.99, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(
        11,
        'Rock Juice',
        Category.Breverage.index,
        "mamak king2",
        "img2.jpg",
        12.00,
        true,
        null,
        null,
        DateTime.now()));
    await bean.insert(new Product.make(4, 'No Juice', Category.Breverage.index,
        "mamak king2", "img2.jpg", 1.00, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(5, 'Yes Juice', Category.Breverage.index,
        "mamak king2", "img2.jpg", 88.00, true, null, null, DateTime.now()));
    await bean.insert(new Product.make(
        6,
        'Heaven Juice',
        Category.Breverage.index,
        "mamak king2",
        "img2.jpg",
        199.00,
        true,
        null,
        null,
        DateTime.now()));

    int id3 = await bean.insert(new Product.make(
        3,
        'Nasi Lemak Special',
        Category.Special.index,
        "mamak king",
        "img3.jpg",
        64.00,
        true,
        null,
        null,
        DateTime.now()));

    // int orderid =  await orderBean.insert(new Order.make(null,"nothing",1,null,OrderStatus.Order.index, DateTime.now()));

    // await orderItemBean.insert(new OrderItem.make(null,orderid,id3,"kurang manis",1,0,OrderStatus.Order.index, DateTime.now()));

    // Product post1 = await bean.find(id3);
    // print(post1.description);

    // getAllProductByCategory(Category.Noodle);
    // _adapter.close();
  } catch (e) {
    print("errorDatabase" + e.toString());
    //_adapter.close();
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.stream}) : super(key: key);

  final Stream<int> stream;

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  MyHomePageState myState = MyHomePageState();

  @override
  MyHomePageState createState() {
    return myState;
  }
}

class MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  static const platform = const MethodChannel('samples.flutter.io/battery');
  static const platform2 =
      const MethodChannel('samples.flutter.io/orientation');

  // Get battery level.
  String _batteryLevel = 'Unknown battery level.';

  Future<void> _getBatteryLevel() async {
    String batteryLevel;
    try {
      final int result = await platform.invokeMethod('getBatteryLevel');
      batteryLevel = 'Battery level at $result % .';
    } on PlatformException catch (e) {
      batteryLevel = "Failed to get battery level: '${e.message}'.";
    }

    print("BATTERY " + batteryLevel);
    setState(() {
      _batteryLevel = batteryLevel;
    });
  }

  Future<void> setOrientation() async {
    try {
      final int result = await platform2.invokeMethod('setOrientationMode');
    } on PlatformException catch (e) {
      print("Failed to get battery level: '${e.message}'.");
    }

    print("BATTERY ");
  }

  String _platformVersion;
  Permission permission = Permission.Camera;

  initplatform() async {
    String platfrom;
    try {
      platfrom = await SimplePermissions.platformVersion;
    } on PlatformException {
      platfrom = "platform not found";
    }
//if object is removed from the tree.
    if (!mounted) return;
//otherwise set the platform to our _platformversion global variable
    setState(() => _platformVersion = platfrom);
  }

  void checkpermission() async {
    bool result = await SimplePermissions.checkPermission(permission);
    print("permission is " + result.toString());
  }

  void requestPermission() async {
    var result = await SimplePermissions.requestPermission(permission);
    print("request :" + result.toString());
  }

  void getstatus() async {
    final result = await SimplePermissions.getPermissionStatus(permission);
    print("permission status is :" + result.toString());
  }

  @override
  initState() {
    super.initState();
    //_getBatteryLevel();
    _firebaseMessaging.requestNotificationPermissions();
    setOrientation();
    initplatform();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
  }

  bool _obscureText = true;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _username;
  String _password;

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    setupDatabase();
    return buildLandscapeMode();
  }

  userLoginPressed() async {
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
      print(_username);
      print(_password);

      PosUser posUser = await userLogin(_username, _password);

      if (posUser != null) {
        print("LoginDONE" + posUser.username);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => MyApp()),
        );
      }
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  String barcode = "";

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      print("barcode : " + barcode);
      setState(() => this.barcode = barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
          print(barcode);
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
        print("error : " + barcode);
      }
    } on FormatException {
      setState(() => this.barcode =
          'null (User returned using the "back"-button before scanning anything. Result)');
      print("FormatException : " + barcode);
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
      print("UnknownException : " + barcode);
    }
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }

  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  buildLandscapeMode() {
//    StreamBuilder(
//        stream: widget.stream,
//        builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
//
//          //return snapshot.hasData ? Text(snapshot.data.toString()) : Text('nodata');

    return Scaffold(
      body: new Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(image: new AssetImage("assets/bg.jpg")  ,    fit: BoxFit.cover,),
          ),
          child: Center(
              child: new Container(
            height: 300,
            width: 300,
            child:
//        new Material(
//          elevation: 0,
//          child:
                new Form(
                    key: _formKey,
                    autovalidate: _autoValidate,
                    child: new Column(children: <Widget>[
                      new Padding(
                          padding: EdgeInsets.all(16),
                          child: new TextFormField(

                            style: new TextStyle(color: Colors.white ),
                            keyboardType: TextInputType.text,
                            decoration: new InputDecoration(
                              hintStyle: TextStyle(color: Colors.white ),
                              hintText: 'Username',
                                prefixStyle:new TextStyle(color: Colors.white ),
                                contentPadding: new EdgeInsets.symmetric(
                                  vertical: 8.0, horizontal: 20.0),
                              enabledBorder: const OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(30.0),
                                ),
                              ),
                              border: const OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(30.0),
                                ),
                                // width: 0.0 produces a thin "hairline" border
                                borderSide: const BorderSide(
                                    color: Colors.white, width: 0.0),
                              ),
                            ),
                            validator: (val) =>
                                val.length < 3 ? 'username too short.' : null,
                            onSaved: (val) => _username = val,
                          )),
                      new Stack(
                        children: <Widget>[
                          new Padding(
                              padding: EdgeInsets.all(16),
                              child: new TextFormField(
                                style: new TextStyle(color: Colors.white ),
                                keyboardType: TextInputType.text,
                                decoration: new InputDecoration(
                                  hintStyle: TextStyle(color: Colors.white ),
                                  hintText: 'Password',
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 8.0, horizontal: 20.0),
                                  enabledBorder: const OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(30.0),
                                ),
                              ),
                                  border: const OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(30.0),
                                    ),
                                  ),
                                ),
                                validator: (val) => val.length < 3
                                    ? 'Password too short.'
                                    : null,
                                onSaved: (val) => _password = val,
                                obscureText: _obscureText,
                              )),
                          new Positioned(
                              right: 0.0,
                              top: 9,
                              child: new FlatButton(
                                  onPressed: _toggle,
                                  child: new Text(
                                    _obscureText ? "Show" : "Hide",
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 10.0,
                                        fontWeight: FontWeight.normal),
                                  ))),
                        ],
                      ),
                      new Padding(
                          padding: EdgeInsets.all(16),
                          child: new RaisedButton(
                            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                            onPressed: userLoginPressed,
                            color: Colors.green,
                            padding: EdgeInsets.all(4.0),
                            child: Center(
                              // Replace with a Row for horizontal icon + text
                              child: Text("LOGIN",
                                  style: new TextStyle(color: Colors.white)),
                            ),
                          )),
//                new RaisedButton(
//                  onPressed: requestPermission,
//                  color: Colors.orange,
//                  child: new Text("request the permission"),
//                ),
//                RaisedButton(
//                    color: Colors.blue,
//                    textColor: Colors.white,
//                    splashColor: Colors.blueGrey,
//                    onPressed: scan,
//                    child: const Text('START CAMERA SCAN')
//                ),
                    ])),
            // ),
          ))),
    );
    //  });
  }
}

class WebViewExample extends StatelessWidget {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter WebView example'),
        // This drop down menu demonstrates that Flutter widgets can be shown over the web view.
        actions: <Widget>[
          NavigationControls(_controller.future),
          SampleMenu(_controller.future),
        ],
      ),
      // We're using a Builder here so we have a context that is below the Scaffold
      // to allow calling Scaffold.of(context) so we can show a snackbar.
      body: Builder(builder: (BuildContext context) {
        return WebView(
          initialUrl: 'https://flutter.io',
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
          javascriptChannels: <JavascriptChannel>[
            _toasterJavascriptChannel(context),
          ].toSet(),
        );
      }),
      floatingActionButton: favoriteButton(),
    );
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }

  Widget favoriteButton() {
    return FutureBuilder<WebViewController>(
        future: _controller.future,
        builder: (BuildContext context,
            AsyncSnapshot<WebViewController> controller) {
          if (controller.hasData) {
            return FloatingActionButton(
              onPressed: () async {
                final String url = await controller.data.currentUrl();
                Scaffold.of(context).showSnackBar(
                  SnackBar(content: Text('Favorited $url')),
                );
              },
              child: const Icon(Icons.favorite),
            );
          }
          return Container();
        });
  }
}

enum MenuOptions {
  showUserAgent,
  toast,
  listCookies,
  clearCookies,
}

class SampleMenu extends StatelessWidget {
  SampleMenu(this.controller);

  final Future<WebViewController> controller;
  final CookieManager cookieManager = CookieManager();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: controller,
      builder:
          (BuildContext context, AsyncSnapshot<WebViewController> controller) {
        return PopupMenuButton<MenuOptions>(
          onSelected: (MenuOptions value) {
            switch (value) {
              case MenuOptions.showUserAgent:
                _onShowUserAgent(controller.data, context);
                break;
              case MenuOptions.toast:
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Text('You selected: $value'),
                  ),
                );
                break;
              case MenuOptions.listCookies:
                _onListCookies(controller.data, context);
                break;
              case MenuOptions.clearCookies:
                _onClearCookies(context);
                break;
            }
          },
          itemBuilder: (BuildContext context) => <PopupMenuItem<MenuOptions>>[
                PopupMenuItem<MenuOptions>(
                  value: MenuOptions.showUserAgent,
                  child: const Text('Show user agent'),
                  enabled: controller.hasData,
                ),
                const PopupMenuItem<MenuOptions>(
                  value: MenuOptions.toast,
                  child: Text('Make a toast'),
                ),
                const PopupMenuItem<MenuOptions>(
                  value: MenuOptions.listCookies,
                  child: Text('List cookies'),
                ),
                const PopupMenuItem<MenuOptions>(
                  value: MenuOptions.clearCookies,
                  child: Text('Clear cookies'),
                ),
              ],
        );
      },
    );
  }

  void _onShowUserAgent(
      WebViewController controller, BuildContext context) async {
    // Send a message with the user agent string to the Toaster JavaScript channel we registered
    // with the WebView.
    controller.evaluateJavascript(
        'Toaster.postMessage("User Agent: " + navigator.userAgent);');
  }

  void _onListCookies(
      WebViewController controller, BuildContext context) async {
    final String cookies =
        await controller.evaluateJavascript('document.cookie');
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Text('Cookies:'),
          _getCookieList(cookies),
        ],
      ),
    ));
  }

  void _onClearCookies(BuildContext context) async {
    final bool hadCookies = await cookieManager.clearCookies();
    String message = 'There were cookies. Now, they are gone!';
    if (!hadCookies) {
      message = 'There are no cookies.';
    }
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  Widget _getCookieList(String cookies) {
    if (cookies == null || cookies == '""') {
      return Container();
    }
    final List<String> cookieList = cookies.split(';');
    final Iterable<Text> cookieWidgets =
        cookieList.map((String cookie) => Text(cookie));
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: cookieWidgets.toList(),
    );
  }
}

class NavigationControls extends StatelessWidget {
  const NavigationControls(this._webViewControllerFuture)
      : assert(_webViewControllerFuture != null);

  final Future<WebViewController> _webViewControllerFuture;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: _webViewControllerFuture,
      builder:
          (BuildContext context, AsyncSnapshot<WebViewController> snapshot) {
        final bool webViewReady =
            snapshot.connectionState == ConnectionState.done;
        final WebViewController controller = snapshot.data;
        return Row(
          children: <Widget>[
            IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: !webViewReady
                  ? null
                  : () async {
                      if (await controller.canGoBack()) {
                        controller.goBack();
                      } else {
                        Scaffold.of(context).showSnackBar(
                          const SnackBar(content: Text("No back history item")),
                        );
                        return;
                      }
                    },
            ),
            IconButton(
              icon: const Icon(Icons.arrow_forward_ios),
              onPressed: !webViewReady
                  ? null
                  : () async {
                      if (await controller.canGoForward()) {
                        controller.goForward();
                      } else {
                        Scaffold.of(context).showSnackBar(
                          const SnackBar(
                              content: Text("No forward history item")),
                        );
                        return;
                      }
                    },
            ),
            IconButton(
              icon: const Icon(Icons.replay),
              onPressed: !webViewReady
                  ? null
                  : () {
                      controller.reload();
                    },
            ),
          ],
        );
      },
    );
  }
}
